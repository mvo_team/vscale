import 'package:test/test.dart';
import 'package:vscale_io/data/models/models.dart';
import 'package:vscale_io/data/models/user_token_entity.dart';
import 'package:vscale_io/data/provider/network/network_provider.dart';
import 'package:vscale_io/di/injector.dart';
import 'package:vscale_io/domain/repositories/account_repository.dart';
import 'package:vscale_io/domain/repositories/server_repository.dart';

void main() {
  AccountRepository repository = Injector.inject();
  ScaletRepository scarletRepository = Injector.inject();
  UserTokenEntity testToken = UserTokenEntity(
      name: "testToken",
      token:
          "7c2d9abc6e73952a2faaee904ab603222a793aefcf5d25c6d8d12351f9abdaee");

  test("test get account", () {
    repository.setToken(testToken);
    expect(repository.getAccount().catchError((onError) => print(onError)),
        completion(equals(isInstanceOf<AccountEntity>())));
  });

  test("test token", () {
    repository.setToken(testToken);
    expect(repository.getToken(), completion(equals(testToken)));
  });

  test("test list server", () {
    repository.setToken(testToken);
    expect(scarletRepository.getScaletList(),
        completion(equals(isInstanceOf<List<ScaletEntity>>())));
  });
}
