import 'package:vscale_io/data/fabric/fabric_pattern.dart';
import 'package:vscale_io/data/models/models.dart';

class ScaletFabric implements FabricPattern<ScaletEntity> {
  @override
  ScaletEntity create(json) {
    return ScaletEntity.fromJson(json);
  }
}
