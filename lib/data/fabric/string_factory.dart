import 'package:vscale_io/data/fabric/fabric_pattern.dart';

class StringFactory extends FabricPattern<String> {
  @override
  String create(json) {
    return json.toString();
  }
}
