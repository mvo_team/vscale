import 'package:vscale_io/data/fabric/fabric_pattern.dart';

class EntityConverter {
  Map<Type, FabricPattern> _factoryMap = Map();
  static EntityConverter _instance;

  static EntityConverter getInstance() {
    if (_instance == null) {
      _instance = EntityConverter();
    }
    return _instance;
  }

  T create<T>(json) {
    var factory = _factoryMap[T];
    if (factory == null) {
      throw Exception("Factory for type [$T] not found!!!");
    }

    return factory.create(json);
  }

  register<T>(FabricPattern<T> fabric) {
    _factoryMap.putIfAbsent(T, () => fabric);
  }
}

List<T> parseList<T>(dynamic json) {
  print(json);
  return (json == null)
      ? List()
      : (json as List)
          .map<T>((data) => EntityConverter.getInstance().create<T>(data))
          .toList();
}
