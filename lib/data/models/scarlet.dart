import 'package:vscale_io/data/converters.dart';
import 'package:vscale_io/data/fabric/fabric_pattern.dart';
import 'package:vscale_io/data/models/address_entity.dart';
import 'package:vscale_io/data/models/key_entity.dart';
import 'package:vscale_io/data/models/scale_status.dart';
import 'package:vscale_io/data/models/tag_entity.dart';

class ScaletEntity {
  final String hostname;
  final bool isLocked;
  final String location;
  final String rplan;
  final String name;
  final bool isActive;
  final List<KeyEntity> keys;
  final List<TagEntity> tags;
  final AddressEntity publicAddress;
  ScaletStatusEntity status;
  final String madeFrom;
  final String ctid;
  final AddressEntity privateAddress;

  ScaletEntity(
      {this.hostname,
      this.isLocked,
      this.location,
      this.rplan,
      this.name,
      this.isActive,
      this.keys,
      this.tags,
      this.publicAddress,
      this.status,
      this.madeFrom,
      this.ctid,
      this.privateAddress})
      : super();

  @override
  toString() {
    return "hostname: ${this.hostname},\n" +
        "isLocked: ${this.isLocked},\n" +
        "location: ${this.location},\n" +
        "rplan: ${this.rplan},\n" +
        "name: ${this.name},\n" +
        "isActive: ${this.isActive},\n" +
        "keys: ${this.keys},\n" +
        "tags: ${this.tags},\n" +
        "publicAddress: ${this.publicAddress},\n" +
        "status: ${this.status},\n" +
        "madeFrom: ${this.madeFrom},\n" +
        "ctid: ${this.ctid},\n" +
        "privateAddress: ${this.privateAddress},\n\n";
  }

  factory ScaletEntity.fromJson(dynamic json) {
    var entity = ScaletEntity(
      hostname: json['hostname'].toString() ?? null,
      isLocked: json['locked'] ?? false,
      location: json['location'].toString() ?? null,
      rplan: json['rplan'].toString() ?? null,
      name: json['name'].toString() ?? null,
      isActive: json['active'] ?? false,
      keys: parseList(json['keys']) ?? Iterable.empty(),
      tags: parseList(json['tags']) ?? Iterable.empty(),
      publicAddress: AddressEntity.fromJson(json['public_address']) ?? null,
      status: ScaletStatusEntity.parseStatus(json['status'].toString() ?? null),
      madeFrom: json['made_from'].toString() ?? null,
      ctid: json['ctid'].toString() ?? null,
      privateAddress: AddressEntity.fromJson(json['private_address']),
    );

    print(entity.toString());
    return entity;
  }
}

class ScaleFabric extends FabricPattern<ScaletEntity> {
  @override
  ScaletEntity create(json) {
    return ScaletEntity.fromJson(json);
  }
}
