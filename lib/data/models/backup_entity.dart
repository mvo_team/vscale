import 'package:vscale_io/data/fabric/fabric_pattern.dart';
import 'package:vscale_io/data/models/backup_status.dart';

class BackupEntity {
  final String id;
  final String tempalate;
  final String name;
  final String scalet;
  final bool isActive;
  final BackupStatusEntity status; //TODO create enum
  final double size;
  final bool isLocked;
  final String location;
  final String created;

  BackupEntity(
      {this.id,
      this.tempalate,
      this.name,
      this.scalet,
      this.isActive,
      this.status,
      this.size,
      this.isLocked,
      this.location,
      this.created});

  factory BackupEntity.fromJson(dynamic json) {
    return BackupEntity(
      id: json['id'].toString() ?? null,
      tempalate: json['template'].toString() ?? null,
      name: json['name'].toString() ?? null,
      scalet: json['scalet'].toString() ?? null,
      isActive: json['active'] ?? false,
      status: BackupStatusEntity.parseStatus(json['status'].toString() ?? null),
      size: json['size'] ?? 0.0,
      isLocked: json['locked'] ?? false,
      location: json['location'].toString() ?? null,
      created: json['created'].toString() ?? null,
    );
  }
}


class BackupFabric extends FabricPattern<BackupEntity> {
  @override
  BackupEntity create(json) {
    return BackupEntity.fromJson(json);
  }
}