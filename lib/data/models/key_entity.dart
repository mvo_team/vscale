import 'package:vscale_io/data/fabric/fabric_pattern.dart';

class KeyEntity {
  final String name;
  final String id;

  KeyEntity({this.name, this.id});

  factory KeyEntity.fromJson(dynamic json) {
    return KeyEntity(
      name: json['name'].toString() ?? null,
      id: json['id'].toString() ?? null,
    );
  }

  @override
  toString() {
    return "id: ${this.id},\n" + "name: ${this.name}\n\n";
  }
}

class KeyFactory extends FabricPattern<KeyEntity> {
  @override
  KeyEntity create(json) {
    return KeyEntity.fromJson(json);
  }
}
