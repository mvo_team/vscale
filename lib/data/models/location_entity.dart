import 'package:vscale_io/data/converters.dart';
import 'package:vscale_io/data/fabric/fabric_pattern.dart';

class LocationEntity {
  final String id;
  final String description;
  final List<String> templates;
  final List<String> rplans;
  final bool isActive;
  final bool privateNetworking;

  LocationEntity(
      {this.id,
      this.description,
      this.templates,
      this.rplans,
      this.isActive,
      this.privateNetworking});

  factory LocationEntity.fromJson(dynamic json) {
    return LocationEntity(
      id: json['id'].toString() ?? null,
      description: json['description'].toString() ?? null,
      templates: parseList(json['templates']) ?? List(),
      rplans: parseList(json['rplans']) ?? List(),
      isActive: json['active'] ?? false,
      privateNetworking: json['private_networking'] ?? false,
    );
  }
}

class LocationFactory extends FabricPattern<LocationEntity> {
  @override
  LocationEntity create(json) {
    return LocationEntity.fromJson(json);
  }
}
