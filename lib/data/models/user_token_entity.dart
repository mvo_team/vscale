import 'dart:convert';

import 'package:vscale_io/data/fabric/fabric_pattern.dart';

class UserTokenEntity {
  final String name;
  final String token;

  UserTokenEntity({this.name, this.token});

  factory UserTokenEntity.fromJson(dynamic json) {
    print("fromJson $json");
    return UserTokenEntity(
      name: json['name'].toString() ?? null,
      token: json['token'].toString() ?? null,
    );
  }

  factory UserTokenEntity.fromObject(String source) {
    var item = json.decode(source);
    print("fromJsons $item");
    return UserTokenEntity(
      name: item['name'].toString() ?? null,
      token: item['token'].toString() ?? null,
    );
  }

  String toJson() {
    return json.encode({"name": name, "token": token});
  }

  @override
  String toString() {
    return "$name $token";
  }
}

class UserTokenFactory extends FabricPattern<UserTokenEntity> {
  @override
  UserTokenEntity create(json) {
    return UserTokenEntity.fromJson(json);
  }
}
