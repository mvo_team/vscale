import 'package:vscale_io/data/fabric/fabric_pattern.dart';

class AccountEntity {
  final String actdate;
  final String country;
  final String email;
  final String faceId;
  final String id;
  final String locale;
  final String middlename;
  final String mobile;
  final String name;
  final String state;
  final String surname;
  final bool isBlocked;

  AccountEntity(
      {this.actdate,
      this.country,
      this.email,
      this.faceId,
      this.id,
      this.locale,
      this.middlename,
      this.mobile,
      this.name,
      this.state,
      this.surname,
      this.isBlocked});

  factory AccountEntity.fromJson(dynamic json) {
    return AccountEntity(
      actdate: json['actdate'].toString() ?? null,
      country: json['country'].toString() ?? null,
      email: json['email'].toString() ?? null,
      faceId: json['face_id'].toString() ?? null,
      id: json['id'].toString() ?? null,
      locale: json['locale'].toString() ?? null,
      middlename: json['middlename'].toString() ?? null,
      mobile: json['mobile'].toString() ?? null,
      name: json['name'].toString() ?? null,
      state: json['state'].toString() ?? null,
      surname: json['surname'].toString() ?? null,
      isBlocked: json['is_blocked'] ?? false,
    );
  }
}

class AccountFabric extends FabricPattern<AccountEntity> {
  @override
  AccountEntity create(json) {
    return AccountEntity.fromJson(json);
  }
}

/* Example success response

HTTP/1.1 200 OK
{
  "info": {
      "actdate": "2015-07-07 08:16:47.107987",
      "country": "",
      "email": "username@domain.ru",
      "face_id": "1",
      "id": "1001",
      "locale": "ru",
      "middlename": "\u042E\u0437\u0435\u0440\u043E\u0432\u0438\u0447",
      "mobile": "+79901234567",
      "name": "\u041F\u043E\u043B\u044C\u0437\u043E\u0432\u0430\u0442\u0435\u043B\u044C",
      "state": "1",
      "surname": "\u0412\u0441\u043A\u0430\u043B\u0435\u0442\u043E\u0432"
  }
}

*/
