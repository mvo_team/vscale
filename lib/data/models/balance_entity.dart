import 'package:vscale_io/data/fabric/fabric_pattern.dart';

class BalanceEntity {
  final double balance;
  final double bonus;
  final String status;
  final String summ;
  final String unpaid;
  final int userId;

  BalanceEntity(
      {this.balance,
      this.bonus,
      this.status,
      this.summ,
      this.unpaid,
      this.userId});

  factory BalanceEntity.fromJson(dynamic json) {
    return BalanceEntity(
      balance: (json['balance'] ?? 0) / 100.0,
      bonus: (json['bonus'] ?? 0) / 100.0,
      status: json['status'].toString() ?? null,
      summ: json['summ'].toString() ?? null,
      unpaid: json['unpaid'].toString() ?? null,
      userId: json['user_id'] ?? 0,
    );
  }
}

class BalanceFactory extends FabricPattern<BalanceEntity> {
  @override
  BalanceEntity create(json) {
    return BalanceEntity.fromJson(json);
  }
}
