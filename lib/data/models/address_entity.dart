import 'package:vscale_io/data/fabric/fabric_pattern.dart';

class AddressEntity {
  final String netmask;
  final String gateway;
  final String address;

  AddressEntity({this.netmask, this.gateway, this.address});

  factory AddressEntity.fromJson(dynamic json) {
    return AddressEntity(
      netmask: json['netmask'].toString() ?? null,
      gateway: json['gateway'].toString() ?? null,
      address: json['address'].toString() ?? null,
    );
  }

  @override
  toString() {
    return "netmask: ${this.netmask},\n" +
        "gateway: ${this.gateway},\n" +
        "address: ${this.address}\n\n";
  }
}

class AddressFactory extends FabricPattern<AddressEntity> {
  @override
  AddressEntity create(json) {
    return AddressEntity.fromJson(json);
  }
}
