
import 'package:vscale_io/data/fabric/fabric_pattern.dart';

class TagEntity {
  final String name;
  final String id;

  TagEntity({this.name, this.id});

  factory TagEntity.fromJson(dynamic json) {
    return TagEntity(
      name: json['name'].toString() ?? null,
      id: json['id'].toString() ?? null,
    );
  }

  @override
  toString() {
    return "id: ${this.id},\n" + "name: ${this.name}\n\n";
  }
}

class TagFactory extends FabricPattern<TagEntity> {
  @override
  TagEntity create(json) {
    return TagEntity.fromJson(json);
  }

}
