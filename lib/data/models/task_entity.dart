import 'package:vscale_io/data/fabric/fabric_pattern.dart';

class TaskEntity {
  final String location;
  final String dInsert;
  final String id;
  final bool isDone;
  final String scalet;
  final bool isError;
  final String dStart;
  final String method;
  final String dNull;

  TaskEntity(
      {this.location,
      this.dInsert,
      this.id,
      this.isDone,
      this.scalet,
      this.isError,
      this.dStart,
      this.method,
      this.dNull});

  factory TaskEntity.fromJson(dynamic json) {
    return TaskEntity(
      location: json['location'].toString() ?? null,
      dInsert: json['d_insert'].toString() ?? null,
      id: json['id'].toString() ?? null,
      isDone: json['done'] ?? false,
      scalet: json['scalet'].toString() ?? null,
      isError: json['error'] ?? false,
      dStart: json['d_start'].toString() ?? null,
      method: json['method'].toString() ?? null,
      dNull: json['d_null'].toString() ?? null,
    );
  }
}

class TaskFabric extends FabricPattern<TaskEntity> {
  @override
  TaskEntity create(json) {
    return TaskEntity.fromJson(json);
  }
}
