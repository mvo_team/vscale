import 'package:flutter/material.dart';

enum Status { STARTED, STOPPED, BILLING }

class ScaletStatusEntity {
  Color color;
  Status status;
  String title;

  ScaletStatusEntity({this.color, this.status, this.title});

  factory ScaletStatusEntity.parseStatus(String text) {
    Status status = _parseStatus(text);
    Color color = _getColorStatus(status);
    String title = text;
    return ScaletStatusEntity(color: color, status: status, title: title);
  }
}

Status _parseStatus(String status) {
  print("_parseStatus $status");
  switch (status) {
    case "started":
      return Status.STARTED;
      break;
    case "stopped":
      return Status.STOPPED;
      break;
    case "billing":
      return Status.BILLING;
      break;
    default:
      return Status.STOPPED;
      break;
  }
}

Color _getColorStatus(Status status) {
  Color color = Colors.green;
  switch (status) {
    case Status.STARTED:
      color = Colors.green;
      break;
    case Status.STOPPED:
      color = Colors.red;
      break;
    case Status.BILLING:
      color = Colors.pink;
      break;
    default:
      color = Colors.red;
      break;
  }
  return color;
}
