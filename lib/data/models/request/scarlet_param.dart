class ScarletParam {
  final String makeFrom;
  final String rplan;
  final bool doStart;
  final String name;
  final List<String> keys;
  final String password;
  final String location;

  ScarletParam(
      {this.makeFrom,
      this.rplan,
      this.doStart,
      this.name,
      this.keys,
      this.password,
      this.location});
}
