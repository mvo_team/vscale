import 'dart:convert';

import 'package:vscale_io/data/fabric/fabric_pattern.dart';

class PricesEntity {
  final String period;
  final Map<String, Object> items;

  PricesEntity({this.period, this.items});

  factory PricesEntity.fromJson(dynamic data) {
    return PricesEntity(
      period: data['period'].toString() ?? null,
      items: data['default'],
    );
  }
}

class PricesFactory implements FabricPattern<PricesEntity> {
  @override
  PricesEntity create(json) {
    return PricesEntity.fromJson(json);
  }
}
