import 'package:flutter/material.dart';

enum Status { DEFINING, CREATING, UPLOADING, FINISHED }

class BackupStatusEntity {
  final Color color;
  final Status status;
  final String title;

  BackupStatusEntity({this.color, this.status, this.title});

  factory BackupStatusEntity.parseStatus(String text) {
    Status status = _parseStatus(text);
    Color color = _getColorStatus(status);
    String title = text;
    return BackupStatusEntity(color: color, status: status, title: title);
  }
}

Status _parseStatus(String status) {
  print("_parseStatus $status");
  switch (status) {
    case "defining":
      return Status.DEFINING;
      break;
    case "creating":
      return Status.CREATING;
      break;
    case "uploading":
      return Status.UPLOADING;
      break;
    case "finished":
      return Status.FINISHED;
      break;
    default:
      return Status.CREATING;
      break;
  }
}

Color _getColorStatus(Status status) {
  Color color = Colors.green;
  switch (status) {
    case Status.DEFINING:
      color = Colors.green;
      break;
    case Status.CREATING:
      color = Colors.red;
      break;
    case Status.UPLOADING:
      color = Colors.pink;
      break;
    case Status.FINISHED:
      color = Colors.pink;
      break;
  }
  return color;
}
