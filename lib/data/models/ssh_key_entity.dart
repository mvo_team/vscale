import 'package:vscale_io/data/fabric/fabric_pattern.dart';

class SshKeyEntity {
  final String id;
  final String key;
  final String name;

  SshKeyEntity({this.id, this.key, this.name});

  factory SshKeyEntity.fromJson(dynamic json) {
    return SshKeyEntity(
      id: json['id'].toString() ?? null,
      key: json['key'].toString() ?? null,
      name: json['name'].toString() ?? null,
    );
  }
}

class SshKeyFabric extends FabricPattern<SshKeyEntity> {
  @override
  SshKeyEntity create(json) {
    return SshKeyEntity.fromJson(json);
  }
}
