import 'package:vscale_io/data/converters.dart';
import 'package:vscale_io/data/fabric/fabric_pattern.dart';

class ImageEntity {
  final List<String> rplans;
  final bool isActive;
  final int size;
  final List<String> locations;
  final String id;
  final String description;

  ImageEntity(
      {this.rplans,
      this.isActive,
      this.size,
      this.locations,
      this.id,
      this.description});

  factory ImageEntity.fromJson(json) {
    return ImageEntity(
        rplans: parseList(json['rplans']),
        isActive: json['active'] ?? false,
        size: json['size'] ?? 0,
        locations: parseList(json['locations']),
        id: json['id'].toString() ?? null,
        description: json['description'].toString() ?? null);
  }
}

class ImageFactory extends FabricPattern<ImageEntity> {
  @override
  ImageEntity create(dynamic json) {
    return ImageEntity.fromJson(json);
  }
}
