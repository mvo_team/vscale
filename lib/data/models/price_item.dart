import 'package:vscale_io/data/fabric/fabric_pattern.dart';

class PriceItem {
  final int hour;
  final int month;

  PriceItem({this.hour, this.month});

  factory PriceItem.fromJson(dynamic json) {
    return PriceItem(
        hour: json['hour'] ?? 0,
        month: json['month'] ?? 0);
  }
}

class PriceItemFactory implements FabricPattern<PriceItem> {
  @override
  PriceItem create(json) {
    return PriceItem.fromJson(json);
  }
}
