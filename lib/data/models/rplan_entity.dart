import 'package:vscale_io/data/converters.dart';
import 'package:vscale_io/data/fabric/fabric_pattern.dart';

class RplanEntity {
  final String addresses;
  final int cpus;
  final List<String> locations;
  final String id;
  final int memory;
  final List<String> templates;
  final int disk;

  RplanEntity(
      {this.addresses,
      this.cpus,
      this.locations,
      this.id,
      this.memory,
      this.templates,
      this.disk});

  factory RplanEntity.fromJson(dynamic json) {
    return RplanEntity(
      addresses: json['addresses'].toString() ?? null,
      cpus: json['cpus'] ?? 0,
      locations: parseList(json['locations']) ?? List(),
      id: json['id'].toString() ?? null,
      memory: json['memory'] ?? 0,
      templates: parseList(json['templates']) ?? List(),
      disk: json['disk'] ?? 0,
    );
  }
}

class RplanFactory extends FabricPattern<RplanEntity> {
  @override
  RplanEntity create(json) {
    return RplanEntity.fromJson(json);
  }
}
