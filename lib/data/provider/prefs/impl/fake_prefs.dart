import 'dart:async';

import 'package:vscale_io/data/models/user_token_entity.dart';
import 'package:vscale_io/data/provider/prefs/prefs_provider.dart';

class FakePrefs extends PrefsProvider {
  Map<String, Object> map = Map<String, String>();

  @override
  Future<String> getAppId() {
    return Future.value(map["app_id"]);
  }

  @override
  Future<List<UserTokenEntity>> getUserToken() {
    return Future.value(map["token"]);
  }

  @override
  Future<bool> isLoginLastToken() {
    // TODO: implement isLoginLastToken
  }

  @override
  Future<bool> onBoadringComplete() {
    // TODO: implement onBoadringComplete
  }

  @override
  Future<bool> saveToken(UserTokenEntity token) {
    map["token"] = token;
    return Future.value(map["token"] == token);
  }

  @override
  Future<UserTokenEntity> getToken() {
    return Future.value((map["token"] as UserTokenEntity));
  }

  @override
  Future<bool> setToken(UserTokenEntity token) {
    map["token"] = token;
    return Future.value(map["token"] == token);
  }

  @override
  Future<bool> removeToken(UserTokenEntity token) {
    // TODO: implement removeToken
  }
}
