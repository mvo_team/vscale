import 'dart:async';
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/uuid.dart';
import 'package:vscale_io/data/models/user_token_entity.dart';
import 'package:vscale_io/data/provider/prefs/prefs_provider.dart';

class PrefsProviderImpl extends PrefsProvider {
  Future<SharedPreferences> _preferences = SharedPreferences.getInstance();

  @override
  Future<String> getAppId() {
    return _preferences.then((prefs) {
      String appId = prefs.getString("app_id");
      if (appId == null || appId.isEmpty) {
        String uuid = Uuid().v4();
        return prefs.setString("app_id", uuid).then((v) => Future.value(uuid));
      } else {
        return Future.value(appId);
      }
    });
  }

  @override
  Future<bool> saveToken(UserTokenEntity token) {
    return _preferences.then((prefs) {
      var data = prefs.getStringList("user_token_list");
      if (data != null && data.isNotEmpty) {
        var items =
            data.map((item) => UserTokenEntity.fromObject(item)).toList();
        var item = items.where((item) => item.token == token.token);
        if (item != null && item.isNotEmpty) {
          return Future.error(Exception("token exist"));
        } else {
          items.add(token);
          return prefs.setStringList(
              "user_token_list", items.map((item) => item.toJson()).toList());
        }
      } else {
        return prefs.setStringList("user_token_list", [token.toJson()]);
      }
    });
  }

  @override
  Future<bool> onBoadringComplete() {
    return _preferences
        .then((prefs) => prefs.getBool("onboadrding_complete") ?? false);
  }

  @override
  Future<bool> isLoginLastToken() {
    return _preferences
        .then((prefs) => prefs.getBool("is_login_last_token") ?? false);
  }

  @override
  Future<List<UserTokenEntity>> getUserToken() {
    return _preferences
        .then((prefs) => prefs.getStringList("user_token_list"))
        .then((data) {
      print("getUserToken $data");
      if (data != null) {
        return data
            .map((item) => UserTokenEntity.fromJson(json.decode(item)))
            .toList();
      } else {
        return Future.value(List<UserTokenEntity>());
      }
    });
  }

  @override
  Future<UserTokenEntity> getToken() {
    return _preferences
        .then((prefs) => prefs.getString("token"))
        .then((token) => UserTokenEntity.fromJson(json.decode(token)))
        .then((value) {
      print("getToken $value");
      return value;
    });
  }

  @override
  Future<bool> setToken(UserTokenEntity token) {
    return _preferences
        .then((prefs) => prefs.setString("token", token.toJson()))
        .then((value) {
      print("setToken $value $token");
      return value;
    });
  }

  @override
  Future<bool> removeToken(UserTokenEntity token) {
    return getUserToken().then((list) {
      if (list != null && list.isNotEmpty) {
        var items = list.where((item) => item.token != token.token).toList();
        return _setListToken(items);
      }
    });
  }

  Future<bool> _setListToken(List<UserTokenEntity> list) {
    return _preferences.then((prefs) {
      prefs.setStringList("user_token_list",
          list.map((item) => item.toJson()).toList() ?? List());
    });
  }
}
