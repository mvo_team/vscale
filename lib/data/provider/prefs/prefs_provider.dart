import 'dart:async';

import 'package:vscale_io/data/models/user_token_entity.dart';

abstract class PrefsProvider {
  Future<String> getAppId();
  Future<bool> saveToken(UserTokenEntity entity);
  Future<UserTokenEntity> getToken();
  Future<bool> setToken(UserTokenEntity token);
  Future<bool> onBoadringComplete();
  Future<bool> isLoginLastToken();
  Future<bool> removeToken(UserTokenEntity token);
  Future<List<UserTokenEntity>> getUserToken();
}
