import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:vscale_io/data/converters.dart';
import 'package:vscale_io/data/models/models.dart';
import 'package:vscale_io/data/provider/network/AuthorizationException.dart';
import 'package:vscale_io/data/provider/network/api.dart';
import 'package:vscale_io/data/provider/network/network_provider.dart';
import 'package:vscale_io/data/provider/prefs/prefs_provider.dart';

class NetworkProviderImpl implements NetworkProvider {
  http.Client _client;
  PrefsProvider _prefs;
  EntityConverter _converter;

  NetworkProviderImpl(
      http.Client client, PrefsProvider _prefs, EntityConverter converter) {
    this._client = client;
    this._prefs = _prefs;
    this._converter = converter;
  }

  @override
  Future<AccountEntity> getAccount() {
    return _prefs
        .getToken()
        .then((token) {
          print("get token $token");
          return _client.get(_generateMethod(Api.ACCOUNT),
              headers: {"X-Token": token.token});
        })
        .then(_handleResponse)
        .then((data) => _converter.create<AccountEntity>(data['info']));
  }

  @override
  Future<List<ScaletEntity>> getScaletList() {
    return _prefs
        .getToken()
        .then((token) {
          return _client.get(_generateMethod(Api.SCALETS),
              headers: {"X-Token": token.token});
        })
        .then(_handleResponse)
        .then((data) => parseList<ScaletEntity>(data));
  }

  @override
  Future<ScaletEntity> createScalet(ScarletParam param) {
    return _prefs
        .getToken()
        .then((token) {
          return _client.post(_generateMethod(Api.SCALETS),
              headers: {"X-Token": token.token},
              body: json.encode({
                "make_from": param.makeFrom,
                "rplan": param.rplan,
                "do_start": param.doStart,
                "name": param.name,
                "key": param.keys,
                "password": param.password,
                "location": param.location
              }));
        })
        .then(_handleResponse)
        .then((data) => _converter.create<ScaletEntity>(data));
  }

  @override
  Future<ScaletEntity> getDetailScalet(String ctid) {
    return _prefs
        .getToken()
        .then((token) {
          return _client.get(
              _generateMethod(Api.SCALETS_DETAIL, params: {":ctid": ctid}),
              headers: {"X-Token": token.token});
        })
        .then(_handleResponse)
        .then((data) => _converter.create<ScaletEntity>(data));
  }

  @override
  Future<ScaletEntity> restarstScalets(String ctid) {
    return _prefs
        .getToken()
        .then((token) {
          return _client.patch(
              _generateMethod(Api.SCALETS_RESTART, params: {":ctid": ctid}),
              headers: {"X-Token": token.token});
        })
        .then(_handleResponse)
        .then((data) => _converter.create<ScaletEntity>(data));
  }

  @override
  Future<ScaletEntity> rebuild(String ctid, String password) {
    return _prefs
        .getToken()
        .then((token) {
          return _client.patch(
              _generateMethod(Api.SCALETS_REBUILD, params: {":ctid": ctid}),
              headers: {"X-Token": token.token},
              body: json.encode({"password": password}));
        })
        .then(_handleResponse)
        .then((data) => _converter.create<ScaletEntity>(data));
  }

  @override
  Future<ScaletEntity> stopScalet(String ctid) {
    return _prefs
        .getToken()
        .then((token) {
          return _client.patch(
              _generateMethod(Api.SCALETS_STOP, params: {":ctid": ctid}),
              headers: {"X-Token": token.token});
        })
        .then(_handleResponse)
        .then((data) => _converter.create<ScaletEntity>(data));
  }

  @override
  Future<ScaletEntity> startScalet(String ctid) {
    return _prefs
        .getToken()
        .then((token) {
          return _client.patch(
              _generateMethod(Api.SCALETS_START, params: {":ctid": ctid}),
              headers: {"X-Token": token.token});
        })
        .then(_handleResponse)
        .then((data) => _converter.create<ScaletEntity>(data));
  }

  @override
  Future<ScaletEntity> upgradeScalet(String ctid, String rplan) {
    return _prefs
        .getToken()
        .then((token) {
          return _client.post(
              _generateMethod(Api.SCALETS_UPGRADE, params: {":ctid": ctid}),
              headers: {"X-Token": token.token},
              encoding: Encoding.getByName('utf-8'),
              body: json.encode({"rplan": rplan}));
        })
        .then(_handleResponse)
        .then((data) => _converter.create<ScaletEntity>(data));
  }

  @override
  Future<ScaletEntity> removeScalet(String ctid) {
    return _prefs
        .getToken()
        .then((token) {
          return _client.delete(
              _generateMethod(Api.SCALETS_DETAIL, params: {":ctid": ctid}),
              headers: {"X-Token": token.token});
        })
        .then(_handleResponse)
        .then((data) => _converter.create<ScaletEntity>(data));
  }

  @override
  Future<List<TaskEntity>> getListTask() {
    return _prefs
        .getToken()
        .then((token) {
          return _client.get(_generateMethod(Api.TASKS),
              headers: {"X-Token": token.token});
        })
        .then(_handleResponse)
        .then((data) => parseList<TaskEntity>(data));
  }

  @override
  Future<List<BackupEntity>> getListBackup() {
    return _prefs
        .getToken()
        .then((token) {
          return _client.get(_generateMethod(Api.BACKUPS),
              headers: {"X-Token": token.token});
        })
        .then(_handleResponse)
        .then((data) => parseList<BackupEntity>(data));
  }

  @override
  Future<ScaletEntity> addSshKey(String ctid, String ssh) {
    return _prefs
        .getToken()
        .then((token) {
          return _client.patch(
              _generateMethod(Api.SCALET_SSH, params: {":ctid": ctid}),
              headers: {
                "X-Token": token.token
              },
              body: json.encode({
                "keys": [ssh]
              }));
        })
        .then(_handleResponse)
        .then((data) => _converter.create<ScaletEntity>(data));
  }

  @override
  Future<BackupEntity> createBackup(String ctid, String name) {
    return _prefs
        .getToken()
        .then((token) {
          return _client.post(
              _generateMethod(Api.BACKUP, params: {":ctid": ctid}),
              headers: {
                "X-Token": token.token,
                "Content-Type": "application/json"
              },
              body: json.encode({"name": name}),
              encoding: Utf8Codec(allowMalformed: true));
        })
        .then(_handleResponse)
        .then((data) => _converter.create<BackupEntity>(data));
  }

  @override
  Future<BackupEntity> rebuildScalet(String ctid, String idBackup) {
    return _prefs
        .getToken()
        .then((token) {
          return _client.patch(
              _generateMethod(Api.SCALETS_REBUILD, params: {":ctid": ctid}),
              headers: {"X-Token": token.token},
              body: json.encode({"make_from": idBackup}));
        })
        .then(_handleResponse)
        .then((data) => _converter.create<BackupEntity>(data));
  }

  @override
  Future<BalanceEntity> getBalance() {
    return _prefs
        .getToken()
        .then((token) {
          return _client.get(
            _generateMethod(Api.BALANCE),
            headers: {"X-Token": token.token},
          );
        })
        .then(_handleResponse)
        .then((data) => _converter.create<BalanceEntity>(data));
  }

  @override
  Future<SshKeyEntity> createSshKey(String key, String name) {
    // TODO: implement createSshKey
  }

  @override
  Future<SshKeyEntity> deleteSshKey(String keyId) {
    return _prefs
        .getToken()
        .then((token) {
          return _client.delete(
            _generateMethod(Api.SSH_KEY_DELETE, params: {"{:key}": keyId}),
            headers: {"X-Token": token.token},
          );
        })
        .then(_handleResponse)
        .then((data) => _converter.create<SshKeyEntity>(data));
  }

  @override
  Future<List<SshKeyEntity>> getSshKeys() {
    return _prefs
        .getToken()
        .then((token) {
          return _client.get(
            _generateMethod(Api.SSH_KEYS),
            headers: {"X-Token": token.token},
          );
        })
        .then(_handleResponse)
        .then((data) => parseList<SshKeyEntity>(data));
  }

  @override
  Future<List<LocationEntity>> getLocations() {
    return _prefs
        .getToken()
        .then((token) {
          return _client.get(
            _generateMethod(Api.LOCATIONS),
            headers: {"X-Token": token.token},
          );
        })
        .then(_handleResponse)
        .then((data) => parseList<LocationEntity>(data));
  }

  @override
  Future<List<RplanEntity>> getRplans() {
    return _prefs
        .getToken()
        .then((token) {
          return _client.get(
            _generateMethod(Api.RPLANS),
            headers: {"X-Token": token.token},
          );
        })
        .then(_handleResponse)
        .then((data) => parseList<RplanEntity>(data));
  }

  @override
  Future<List<ImageEntity>> getImages() {
    return _prefs
        .getToken()
        .then((token) {
          return _client.get(
            _generateMethod(Api.IMAGES),
            headers: {"X-Token": token.token},
          );
        })
        .then(_handleResponse)
        .then((data) => parseList<ImageEntity>(data));
  }

  @override
  Future<PricesEntity> getPrices() {
    return _prefs
        .getToken()
        .then((token) {
          return _client.get(
            _generateMethod(Api.PRICES),
            headers: {"X-Token": token.token},
          );
        })
        .then(_handleResponse)
        .then((data) => _converter.create<PricesEntity>(data));
  }
}

Map<String, String> _generateParams(
    Iterable<MapEntry<String, String>> newEntries) {
  Map<String, String> params = Map();
  params.addEntries(newEntries);
  return params;
}

Future<dynamic> _handleResponse(http.Response response) {
  return Future.value(response).then((Response res) {
    print(res.request);
    print(res.statusCode);
    print(res.reasonPhrase);
    print(res.body);
    if (res.statusCode == 200) {
      return res.body;
    } else if ([403, 404].contains(res.statusCode)) {
      throw AuthorizationException(res.body);
    } else {
      throw Exception(res.body);
    }
  }).then((response) => json.decode(response));
}

String _generateMethod(String method,
    {Map<String, String> params, Map<String, String> query}) {
  String delimeter = "/";
  if (method.startsWith("/")) {
    delimeter = "";
  }
  String path = "${Api.HOST}$delimeter$method";
  if (params != null) {
    print("params:");
    params.forEach((key, value) {
      print("\{$key\} = $value");
      path = path.replaceAll("\{$key\}", value);
    });
  }
  if (query != null) {
    print("query:");
    query.forEach((key, value) {
      print("\{$key\} = $value");
      if (path.contains("?")) {
        path += "&$key=$value";
      } else {
        path += "?$key=$value";
      }
    });
  }
  print(path);
  return path;
}
