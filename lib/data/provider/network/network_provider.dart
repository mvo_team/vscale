import 'dart:async';
import 'package:vscale_io/data/models/models.dart';

abstract class NetworkProvider {
  Future<AccountEntity> getAccount();

  Future<List<ScaletEntity>> getScaletList();

  Future<ScaletEntity> createScalet(ScarletParam param);

  Future<ScaletEntity> getDetailScalet(String ctid);

  Future<ScaletEntity> restarstScalets(String ctid);

  Future<ScaletEntity> rebuild(String ctid, String password);

  Future<ScaletEntity> stopScalet(String ctid);

  Future<ScaletEntity> startScalet(String ctid);

  Future<ScaletEntity> upgradeScalet(String ctid, String rplan);

  Future<ScaletEntity> removeScalet(String ctid);

  Future<List<TaskEntity>> getListTask();

  Future<List<BackupEntity>> getListBackup();

  Future<ScaletEntity> addSshKey(String ctid, String ssh);

  Future<BackupEntity> createBackup(String ctid, String name);

  Future<BackupEntity> rebuildScalet(String ctid, String idBackup);

  Future<BalanceEntity> getBalance();

  Future<List<SshKeyEntity>> getSshKeys();

  Future<SshKeyEntity> createSshKey(String key, String name);

  Future<SshKeyEntity> deleteSshKey(String keyId);

  Future<List<RplanEntity>> getRplans();

  Future<List<LocationEntity>> getLocations();

  Future<List<ImageEntity>> getImages();

  Future<PricesEntity> getPrices();
}
