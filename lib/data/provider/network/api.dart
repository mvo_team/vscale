/**
 * Description api vscale.io
 * 
 * Token has been send to headers X-Token : [token:String]
 */
abstract class Api {
  static const String HOST = "https://api.vscale.io";
  static const String VERSION = "/v1";

  //Account
  static const String ACCOUNT = "$VERSION/account";

  //Billing
  static const String BALANCE = "$VERSION/billing/balance";
  static const String PAYMENTS = "$VERSION/billing/payments";
  static const String CONSUMPTION = "$VERSION/billing/consumption";
  static const String PRICES = "$VERSION/billing/prices";

  //Servers
  static const String SCALETS = "$VERSION/scalets"; // server list post|get
  static const String SCALETS_DETAIL = "$VERSION/scalets/{:ctid}"; //detail server get|delete
  static const String SCALETS_RESTART = "$VERSION/scalets/{:ctid}/restart"; //restart server
  static const String SCALETS_REBUILD = "$VERSION/scalets/{:ctid}/rebuild"; //reinstall os server
  static const String SCALETS_STOP = "$VERSION/scalets/{:ctid}/stop"; //stop server
  static const String SCALETS_START = "$VERSION/scalets/{:ctid}/start"; //start server
  static const String SCALETS_UPGRADE = "$VERSION/scalets/{:ctid}/upgrade"; //upgrade server conf
  static const String SCALET_SSH = "$VERSION/scalets/{:ctid}";

  //Task
  static const String TASKS = "$VERSION/tasks"; // task list

  //SSH
  static const String SSH_KEYS = "$VERSION/sshkeys";
  static const String SSH_KEY_DELETE = "$VERSION/sshkeys/{:key}";

  //Backup
  static const String BACKUP = "$VERSION/scalets/{:ctid}/backup";
  static const String BACKUPS = "$VERSION/backups";

  //Info
  static const String LOCATIONS = "$VERSION/locations";
  static const String RPLANS = "$VERSION/rplans";
  static const String IMAGES = "$VERSION/images";
}
