import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

abstract class ThemeColors {
  static const Color MAIN = Colors.white;
  static const Color ACCENT = Color(0xff26bcf1);
  static const Color ACCENT_DARK = Color(0xff199ad2);
  static const Color TEXT_COLOR = Color(0xff424951);
  static const Color HINT_COLOR = Color(0xff8a959b);
  
  static const Color ICON = Color(0xff9aa8b1);
  static const Color BORDER = Color(0xffdbdfe2);
  static const Color BACKGROUD_TAG_COLOR = Color(0xfff2f4f7);
}

abstract class FontStyle {
  static const String TEXT_FAMILY = 'Open Sans,sans-serif';
}

abstract class VscaleTheme {
  static ThemeData appTheme = new ThemeData(
      primaryColor: ThemeColors.MAIN,
      accentColor: ThemeColors.ACCENT,
      backgroundColor: ThemeColors.MAIN,
      canvasColor: ThemeColors.MAIN,
      textSelectionColor: ThemeColors.ACCENT,
      inputDecorationTheme: InputDecorationTheme(
        border: OutlineInputBorder(
            borderSide: BorderSide(color: ThemeColors.BORDER)),
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: ThemeColors.BORDER)),
        focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: ThemeColors.ACCENT)),
      ),
      buttonColor: ThemeColors.ACCENT,
      hintColor: ThemeColors.HINT_COLOR,
      textTheme: TextTheme());
}
