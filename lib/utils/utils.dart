import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:vscale_io/utils/styles.dart';

abstract class Utils {
  static const token = 'hFcX0KvfsHgFzMNx68nurDgj89Q4VCRG';

  static buildRouteNavigation(Widget screen) {
    return PageRouteBuilder(pageBuilder: (BuildContext context, _, __) {
      return screen;
    }, transitionsBuilder: (_, Animation<double> animation, __, Widget child) {
      return new SlideTransition(
        position: Tween<Offset>(
          begin: const Offset(1.0, 0.0),
          end: const Offset(0.0, 0.0),
        ).animate(
            CurvedAnimation(parent: animation, curve: Curves.fastOutSlowIn)),
        child: child,
      );
    });
  }

  static buildMaterialRoute(Widget screen) {
    return MaterialPageRoute(builder: (context) => screen, fullscreenDialog: true);
  }

  static showToast(String message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIos: 1,
    );
  }

  static buildLeading(BuildContext context) {
    return Material(
      color: Colors.transparent,
      shape: CircleBorder(),
      child: Container(
        child: InkWell(
          onTap: () {
            Navigator.of(context).pop(false);
          },
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Image.asset('assets/ic_back.png', width: 24.0, height: 24.0),
          ),
        ),
      ),
    );
  }

  static String getDurationLesson(int duration) {
    int seconds = duration ~/ 1000;
    final int hours = seconds ~/ 3600;
    seconds = seconds % 3600;
    var minutes = seconds ~/ 60;
    seconds = seconds % 60;
    return '$hours hr $minutes min';
  }
}
