import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:vscale_io/data/models/user_token_entity.dart';
import 'package:vscale_io/di/injector.dart';
import 'package:vscale_io/domain/interactor/account_interactor.dart';
import 'package:vscale_io/domain/navigator/screen_navigator.dart';
import 'package:vscale_io/ui/common/widget/token_card.dart';
import 'package:vscale_io/ui/common/widget/token_empty_card.dart';
import 'package:vscale_io/ui/login/confirm_dialog.dart';
import 'package:vscale_io/ui/token/add_token_dialog.dart';

class LoginScreen extends StatefulWidget {
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  List<UserTokenEntity> _items = List();

  AccountInteractor _interactor = Injector.inject<AccountInteractor>();

  @override
  void initState() {
    super.initState();
    _loadToken();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
        onPressed: () => showTokenDialog(
            context, (UserTokenEntity token) => _addToken(token)),
      ),
      appBar: AppBar(
        elevation: 0.0,
        centerTitle: true,
        title: Container(
          child: SvgPicture.asset('assets/images/vscale.svg'),
        ),
      ),
      body: prepareScreen(),
    );
  }

  Widget prepareScreen() {
    if (_items != null && _items.isNotEmpty) {
      return ListView.builder(
        itemCount: _items.length,
        itemBuilder: (BuildContext context, int pos) {
          return TokenCard(
              token: _items[pos],
              selectCallback: (UserTokenEntity token) => _selectToken(token),
              removeCallback: (UserTokenEntity token) => _removeToken(token));
        },
      );
    } else {
      return ListView(
        children: <Widget>[
          TokenEmptyCard((UserTokenEntity token) => _addToken(token))
        ],
      );
    }
  }

  void _loadToken() {
    _interactor.getListToken().then((list) {
      setState(() {
        _items = list ?? List();
      });
    }).catchError((e) => print(e.toString()));
  }

  void _removeToken(UserTokenEntity token) {
    var callback = (bool isRemove) {
      if (isRemove) {
        _interactor.removeToken(token).then((res) {
          _loadToken();
        }).catchError((e) {
          print("error $e");
        });
      }
    };
    showDialog(
        context: context,
        builder: (context) =>
            ConfirmDialog(title: "Remove token?", callback: callback));
  }

  void _selectToken(UserTokenEntity token) {
    _interactor.selectToken(token).then((res) {
      if (res) {
        ScreenNavigator.navigateToMain(context);
      }
    }).catchError((e) {
      print("error $e");
    });
  }

  void _addToken(UserTokenEntity value) {
    print("add token ${value.name} ${value.token}");
    _interactor.addToken(value.name, value.token).then((res) {
      _loadToken();
    }).catchError((e) {
      print("error $e");
    });
  }
}
