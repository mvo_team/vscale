import 'package:flutter/material.dart';

class ConfirmDialog extends StatelessWidget {
  Function(bool isRemove) callback;

  String title;

  ConfirmDialog({this.title, this.callback});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: EdgeInsets.only(left: 16.0, right: 16.0, top: 16.0),
      title: Text(title),
      actions: <Widget>[
        FlatButton(
          onPressed: () => _removeToken(),
          child: Text(
            "YES",
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        FlatButton(
          onPressed: () => _closeDialog(context),
          child: Text("CANCEL",
              style: TextStyle(
                fontWeight: FontWeight.bold,
              )),
        )
      ],
    );
  }

  _removeToken() {
    callback(true);
  }

  _closeDialog(context) {
    Navigator.pop(context);
  }
}
