import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:vscale_io/data/models/account.dart';
import 'package:vscale_io/data/models/balance_entity.dart';
import 'package:vscale_io/di/injector.dart';
import 'package:vscale_io/domain/interactor/billing_interactor.dart';
import 'package:vscale_io/domain/interactor/index.dart';
import 'package:vscale_io/domain/navigator/screen_navigator.dart';
import 'package:vscale_io/ui/account/account_view_model.dart';
import 'package:vscale_io/ui/common/widget/app_bar/app_bar.dart';
import 'package:vscale_io/utils/styles.dart';

class AccountScreen extends StatefulWidget {
  _AccountScreenState createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen>
    with AutomaticKeepAliveClientMixin {
  AccountViewModel vm = AccountViewModel.initState();

  AccountInteractor interactor = Injector.inject();
  BillingInteractor billingInteractor = Injector.inject();

  @override
  void initState() {
    super.initState();
    loadAccount();
  }

  @override
  Widget build(BuildContext context) {
    return AccountContent(vm);
  }

  loadAccount() {
    interactor.getAccount().then((account) {
      vm = AccountViewModel(entity: account, isLoading: false, error: false);
      setState(() {
        this.vm = vm;
      });
      return account;
    }).then((account) => billingInteractor.getBalance().then((balance) {
          vm = AccountViewModel(
              entity: account,
              balance: balance,
              isLoading: false,
              error: false);
          setState(() {
            this.vm = vm;
          });
        }));
  }

  @override
  bool get wantKeepAlive => true;
}

class AccountContent extends StatelessWidget {
  final AccountViewModel viewModel;

  AccountContent(this.viewModel);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: VscaleAppBar(
          actions: <Widget>[
            Material(
              color: Colors.transparent,
              shape: CircleBorder(),
              child: InkWell(
                onTap: () => _navigateToSettings(context),
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Icon(Icons.settings, size: 24.0),
                ),
              ),
            )
          ],
        ),
        body: _prepareContent(context));
  }

  Widget _prepareContent(context) {
    return (!viewModel.isLoading)
        ? _prepareAccountView(context)
        : _prepareLoadingView();
  }

  Widget _prepareAccountView(context) {
    return ListView(
      padding: const EdgeInsets.only(top: 4.0, left: 8.0, right: 8.0),
      children: <Widget>[
        Row(
          children: <Widget>[
            Expanded(child: prepareBalanceInfo(viewModel.balance)),
            prepareBonusInfo(viewModel.balance),
          ],
        ),
        prepareBaseAccountInfo(viewModel.entity),
        prepareLogoutButton(context),
      ],
    );
  }

  Center _prepareLoadingView() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget prepareBonusInfo(BalanceEntity entity) {
    return Container(
        margin: EdgeInsets.only(left: 4.0, top: 8.0, right: 8.0, bottom: 8.0),
        decoration: BoxDecoration(
            color: ThemeColors.BACKGROUD_TAG_COLOR,
            border:
                Border.all(style: BorderStyle.solid, color: ThemeColors.BORDER),
            borderRadius: BorderRadius.circular(0.0)),
        child: (entity != null)
            ? Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Icon(Icons.star),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "Bonus".toUpperCase(),
                      style: TextStyle(
                          fontSize: 16.0, color: ThemeColors.HINT_COLOR),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0, right: 8.0, bottom: 8.0),
                    child: Text(
                      entity.bonus.toString(),
                      style: TextStyle(
                          fontSize: 18.0, fontWeight: FontWeight.bold),
                    ),
                  )
                ],
              )
            : Padding(
                padding: const EdgeInsets.all(16.0),
                child: _prepareLoadingView(),
              ));
  }

  Widget prepareBalanceInfo(BalanceEntity entity) {
    return Container(
        margin: EdgeInsets.only(left: 8.0, top: 8.0, right: 4.0, bottom: 8.0),
        decoration: BoxDecoration(
            color: ThemeColors.BACKGROUD_TAG_COLOR,
            border:
                Border.all(style: BorderStyle.solid, color: ThemeColors.BORDER),
            borderRadius: BorderRadius.circular(0.0)),
        child: (entity != null)
            ? Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Icon(Icons.account_balance_wallet),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "Current balance".toUpperCase(),
                      style: TextStyle(
                          fontSize: 16.0, color: ThemeColors.HINT_COLOR),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0, right: 8.0, bottom: 8.0),
                    child: Text(
                      "${entity.balance} \u20bd",
                      style: TextStyle(
                          fontSize: 18.0, fontWeight: FontWeight.bold),
                    ),
                  )
                ],
              )
            : Padding(
                padding: const EdgeInsets.all(16.0),
                child: _prepareLoadingView(),
              ));
  }

  Widget prepareBaseAccountInfo(AccountEntity entity) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 4.0, horizontal: 8.0),
      decoration: BoxDecoration(
          border:
              Border.all(style: BorderStyle.solid, color: ThemeColors.BORDER),
          borderRadius: BorderRadius.circular(0.0)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: Text("Account",
                  style: TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.bold,
                      fontFamily: "Open Sans,sans-serif")),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("Login"),
                Text(entity.id,
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                        fontFamily: "Open Sans,sans-serif")),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("Email"),
                Text(entity.email),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("Regdate"),
                Text(entity.actdate),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8.0, right: 8.0),
            child: Divider(color: ThemeColors.BORDER, height: 2.0),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "${entity.name} ${entity.middlename} ${entity.surname}",
              style: TextStyle(
                  fontSize: 16.0,
                  fontWeight: FontWeight.bold,
                  fontFamily: "Open Sans,sans-serif"),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("Mobile"),
                Text(entity.mobile),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("Country"),
                Text(entity.country),
              ],
            ),
          ),
        ],
      ),
    );
  }

  _navigateToSettings(BuildContext context) {
    ScreenNavigator.navigateToSettings(context);
  }

  Widget prepareLogoutButton(context) {
    return Container(
      padding: EdgeInsets.all(8.0),
      child: RaisedButton(
        onPressed: () => ScreenNavigator.navigateToLogin(context),
        child: Text(
          "Logout",
          style: TextStyle(color: Colors.white),
        ),
      ),
    );
  }
}
