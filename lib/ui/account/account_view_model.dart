import 'package:meta/meta.dart';
import 'package:vscale_io/data/models/models.dart';

class AccountViewModel {
  final AccountEntity entity;
  final BalanceEntity balance;
  final bool isLoading;
  final bool error;

  AccountViewModel(
      {@required this.entity,
      this.balance,
      @required this.isLoading,
      @required this.error});

  factory AccountViewModel.initState() {
    return AccountViewModel(entity: null, isLoading: true, error: false);
  }
}
