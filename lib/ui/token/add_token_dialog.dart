import 'package:flutter/material.dart';
import 'package:vscale_io/data/models/user_token_entity.dart';
import 'package:vscale_io/utils/styles.dart';

class AddTokenDialog extends StatefulWidget {
  Function(UserTokenEntity token) _callback;

  AddTokenDialog(Function(UserTokenEntity token) callback) {
    this._callback = callback;
  }

  _AddTokenDialogState createState() => _AddTokenDialogState();
}

class _AddTokenDialogState extends State<AddTokenDialog> {
  final nameController = TextEditingController();
  final tokenController = TextEditingController();

  bool _errorName = false;
  bool _errorToken = false;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: EdgeInsets.only(left: 16.0, right: 16.0, top: 16.0),
      title: Text("Add token"),
      actions: <Widget>[
        FlatButton(
          onPressed: () => _addToken(),
          child: Text(
            "ADD",
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        FlatButton(
          onPressed: () => _closeDialog(),
          child: Text("CANCEL",
              style: TextStyle(
                fontWeight: FontWeight.bold,
              )),
        )
      ],
      content: SizedBox(
        width: 320.0,
        height: 160.0,
        child: ListView(
          children: <Widget>[
            TextField(
              controller: nameController,
              decoration: InputDecoration(
                errorText: _errorName ? "Not empty" : null,
                border: UnderlineInputBorder(
                    borderSide: BorderSide(color: ThemeColors.BORDER)),
                enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: ThemeColors.BORDER)),
                focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: ThemeColors.BORDER)),
                contentPadding: EdgeInsets.all(4.0),
                hintText: "Name",
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: TextField(
                controller: tokenController,
                autocorrect: false,
                maxLines: 3,
                decoration: InputDecoration(
                  errorText: _errorToken ? "Not empty" : null,
                  hintText: "token",
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _addToken() {
    var isValidate =
        nameController.text.isNotEmpty && tokenController.text.isNotEmpty;

    if (isValidate) {
      setState(() {
        _errorName = false;
        _errorToken = false;
      });

      widget._callback(UserTokenEntity(
          name: nameController.text, token: tokenController.text));
      Navigator.pop(context);
    } else {
      setState(() {
        _errorName = nameController.text.isEmpty;
        _errorToken = tokenController.text.isEmpty;
      });
    }
  }

  _closeDialog() {
    Navigator.of(context).pop();
  }
}

void showTokenDialog(BuildContext context, dynamic callback) {
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return AddTokenDialog(callback);
      });
}
