import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:vscale_io/di/injector.dart';
import 'package:vscale_io/di/service_locator.dart';
import 'package:vscale_io/domain/interactor/account_interactor.dart';
import 'package:vscale_io/domain/navigator/screen_navigator.dart';
import 'package:vscale_io/utils/styles.dart';

class SplashScreen extends StatefulWidget {
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  AccountInteractor _interactor = Injector.inject<AccountInteractor>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
   _loadToken();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            color: ThemeColors.ACCENT,
            image: DecorationImage(
                image: AssetImage(
                  'assets/images/backgroud_image.png',
                ),
                repeat: ImageRepeat.repeat)),
        child: Center(
          child: Column(
            children: <Widget>[
              Expanded(
                flex: 1,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[Image.asset('assets/images/cloud.png')],
                ),
              ),
              Expanded(
                flex: 2,
                child: Container(
                  decoration: BoxDecoration(color: Colors.white),
                  constraints: BoxConstraints.expand(),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        child: Container(
                          width: 200.0,
                          height: 55.0,
                          child: SvgPicture.asset('assets/images/vscale.svg', fit: BoxFit.contain,)),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void _loadToken() {
    Future.delayed(Duration(seconds: 5))
        .then((v) => _interactor.isLogged())
        .then((isLogged) {
      if (isLogged) {
        ScreenNavigator.navigateToLogin(context);
      } else {
        ScreenNavigator.navigateToLogin(context);
      }
    }).catchError((e) => ScreenNavigator.navigateToLogin(context));
  }
}
