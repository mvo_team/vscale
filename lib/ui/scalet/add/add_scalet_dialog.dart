import 'package:flutter/material.dart';
import 'package:vscale_io/ui/common/widget/app_bar/app_bar.dart';
import 'package:vscale_io/ui/image/image_dialog.dart';
import 'package:vscale_io/ui/locations/location_select_dialog.dart';
import 'package:vscale_io/ui/scalet/add/pages/ImageScalePage.dart';
import 'package:vscale_io/ui/scalet/add/pages/LocationPage.dart';
import 'package:vscale_io/ui/scalet/add/pages/PreferenceScalePage.dart';
import 'package:vscale_io/ui/scalet/add/pages/TarifPage.dart';
import 'package:vscale_io/ui/scalet/detail/component/header_widget.dart';
import 'package:vscale_io/utils/styles.dart';
import 'package:vscale_io/utils/utils.dart';

class AddScaletDialog extends StatefulWidget {
  @override
  _AddScaletDialogState createState() => _AddScaletDialogState();
}

class _AddScaletDialogState extends State<AddScaletDialog> {
  var nameController = TextEditingController();

  bool _errorName = false;

  @override
  Widget build(BuildContext context) {
    var widgets = <Widget>[
      LocationPage(),
      ImageScalePage(),
      TarifPage(),
      PreferenceScalePage()
    ];

    var isVisibleNext = true;
    var isVisibleBack = true;

    return Scaffold(
      appBar: VscaleAppBar(
        title: "Create scalet",
      ),
      body: Container(
        child: Stack(
          children: <Widget>[
            PageView(
              controller: PageController(),
              physics: BouncingScrollPhysics(),
              onPageChanged: (idx) => pageChanges(idx),
              children: widgets,
            ),
            isVisibleNext
                ? Positioned(
                    right: 0,
                    bottom: 0,
                    top: 0,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 24.0, right: 24.0),
                      child: FloatingActionButton(
                        onPressed: () => {},
                        child: Icon(
                          Icons.arrow_right,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  )
                : SizedBox(),
            isVisibleBack
                ? Positioned(
                    left: 0,
                    bottom: 0,
                    top: 0,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 24.0, right: 24.0),
                      child: FloatingActionButton(
                        onPressed: () => {},
                        child: Icon(Icons.arrow_left, color: Colors.white),
                      ),
                    ),
                  )
                : SizedBox()
          ],
        ),
      ),
    );
  }

  /*
  Column(
          children: <Widget>[
            InkWell(onTap: _selectLocation, child: HeaderWidget("Location")),
            TextField(
              controller: nameController,
              decoration: InputDecoration(
                errorText: _errorName ? "Not empty" : null,
                border: UnderlineInputBorder(
                    borderSide: BorderSide(color: ThemeColors.BORDER)),
                enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: ThemeColors.BORDER)),
                focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: ThemeColors.BORDER)),
                contentPadding: EdgeInsets.all(4.0),
                hintText: "Name",
              ),
            ),
            InkWell(onTap: _selectImage, child: HeaderWidget("Make from")),
            HeaderWidget("Plan"),
            HeaderWidget("Auto start"),
            HeaderWidget("SSH keys"),
            HeaderWidget("Password"),
          ],
        )
   */

  void _selectImage() {
    Navigator.of(context).push(Utils.buildMaterialRoute(ImageDialog()));
  }

  void _selectLocation() {
    Navigator.of(context)
        .push(Utils.buildMaterialRoute(LocationSelectDialog()));
  }

  pageChanges(int idx) {}
}
