import 'package:flutter/material.dart';
import 'package:observable/observable.dart';
import 'package:vscale_io/data/models/models.dart';
import 'package:vscale_io/di/injector.dart';
import 'package:vscale_io/domain/interactor/info_interactor.dart';
import 'package:vscale_io/ui/common/vm/base_state.dart';
import 'package:vscale_io/ui/common/widget/app_bar/app_bar.dart';

class LocationPage extends StatefulWidget {
  @override
  _LocationPageState createState() => _LocationPageState();
}

class _LocationPageState extends BaseState<LocationPage> {
  String selectedId;
  List<LocationEntity> values = List();

  InfoInteractor interactor = Injector.inject();

  @override
  void initState() {
    super.initState();
    _loadLocations();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: Text("Select location"),
        automaticallyImplyLeading: false,
      ),
      body: ListView.builder(
          itemCount: values.length,
          itemBuilder: (context, indx) => _prepareItem(values[indx])),
    );
  }

  Widget _prepareItem(LocationEntity entity) {
    return Container(
      child: InkWell(
        onTap: () => _selectItem(entity.id, entity.id),
        child: Row(
          children: <Widget>[
            Radio(
                groupValue: selectedId,
                value: entity.id,
                onChanged: (value) => _selectItem(entity.id, value)),
            Text(entity.description),
          ],
        ),
      ),
    );
  }

  void _selectItem(String id, String value) {
    setState(() {
      if (selectedId == id) {
        selectedId = null;
      } else {
        selectedId = id;
      }
    });
  }

  void setResult() {
    Navigator.of(context)
        .pop(values.firstWhere((item) => item.id == selectedId, orElse: null));
  }

  void _loadLocations() {
    interactor.getLocations().then((values) {
      setState(() {
        this.values = values;
      });
    }).catchError((e) {
      showError(message: e);
    });
  }
}
