import 'package:flutter/material.dart';
import 'package:vscale_io/data/models/models.dart';
import 'package:vscale_io/data/models/scale_status.dart';
import 'package:vscale_io/di/injector.dart';
import 'package:vscale_io/domain/interactor/server_interactor.dart';
import 'package:vscale_io/domain/navigator/screen_navigator.dart';
import 'package:vscale_io/ui/common/widget/app_bar/app_bar.dart';
import 'package:vscale_io/ui/common/widget/scalet_card.dart';
import 'package:vscale_io/ui/scalet/add/add_scalet_dialog.dart';
import 'package:vscale_io/utils/utils.dart';

class ScaletsScreen extends StatefulWidget {
  _ScaletsScreenState createState() => _ScaletsScreenState();
}

class _ScaletsScreenState extends State<ScaletsScreen>
    with AutomaticKeepAliveClientMixin {
  List<ScaletEntity> _items = List();
  bool isLoaded = false;

  ScaletInteractor _interactor = Injector.inject<ScaletInteractor>();

  @override
  void initState() {
    super.initState();
    _loadScalets();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: VscaleAppBar(),
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
        onPressed: _showAddScaleDialog,
      ),
      body: prepareScreen(),
    );
  }

  Widget prepareScreen() {
    if (_items != null && _items.isNotEmpty) {
      return RefreshIndicator(
        onRefresh: _handleRefresh,
        notificationPredicate: (n) => !isLoaded,
        child: ListView.builder(
          physics: const AlwaysScrollableScrollPhysics(),
          itemCount: _items.length,
          itemBuilder: (BuildContext context, int pos) {
            return Container(
              padding: const EdgeInsets.all(8.0),
              child: ScaletCard(
                scalet: _items[pos],
                powerCallback: () => _powerSelect(_items[pos]),
                rebootCallback: () => _rebootSelect(_items[pos]),
                onTap: () => ScreenNavigator.navigateToDetailScalet(
                    context, _items[pos]),
              ),
            );
          },
        ),
      );
    } else {
      return Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: CircularProgressIndicator(),
        ),
      );
    }
  }

  Future<Null> _handleRefresh() {
    return _interactor
        .getListScalets()
        .then((list) {
          setState(() {
            _items = list;
          });
        })
        .catchError((e) => _error(context))
        .then((r) => null);
  }

  void _loadScalets() {
    _interactor.getListScalets().then((list) {
      setState(() {
        _items = list;
      });
    }).catchError((e) => _error(context));
  }

  _error(BuildContext context) {
    Utils.showToast("Error token");
    ScreenNavigator.navigateToLogin(context);
  }

  @protected
  _powerSelect(ScaletEntity item) {
    _getAction(item).then((entity) {
      var it = _items.indexOf(item);
      _items.setAll(it, [entity]);
      setState(() {
        _items = _items;
        isLoaded = false;
      });
    }).catchError((e) => Scaffold.of(context).showSnackBar(SnackBar(
        content: Text("Please wait previous of operation processing"))));
  }

  @protected
  _rebootSelect(ScaletEntity item) {
    _interactor.restartServer(item.ctid).then((entity) {
      var it = _items.indexOf(item);
      _items.setAll(it, [entity]);
      setState(() {
        _items = _items;
        isLoaded = false;
      });
    }).catchError((e) => Scaffold.of(context).showSnackBar(SnackBar(
        content: Text("Please wait previous of operation processing"))));
  }

  Future<ScaletEntity> _getAction(ScaletEntity item) {
    switch (item.status.status) {
      case Status.STARTED:
        return _interactor.stopServer(item.ctid);
        break;
      case Status.STOPPED:
        return _interactor.startServer(item.ctid);
        break;
      case Status.BILLING:
        return _interactor.stopServer(item.ctid);
        break;
    }
  }

  @override
  bool get wantKeepAlive => true;

  void _showAddScaleDialog() {
    ScreenNavigator.showAddScaletDialog(context);
  }
}
