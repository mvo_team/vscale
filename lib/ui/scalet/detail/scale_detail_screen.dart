import 'package:flutter/material.dart';
import 'package:vscale_io/data/models/models.dart';
import 'package:vscale_io/ui/common/widget/app_bar/app_bar.dart';
import 'package:vscale_io/ui/scalet/detail/dialog/change_rplan_dialog.dart';
import 'package:vscale_io/ui/scalet/detail/pages/backup/backup_page.dart';
import 'package:vscale_io/ui/scalet/detail/pages/detail/detail_page.dart';
import 'package:vscale_io/ui/scalet/detail/scalet_tab.dart';
import 'package:vscale_io/utils/styles.dart';

class ScaleDetailScreen extends StatefulWidget {
  ScaletEntity entity;

  ScaleDetailScreen({this.entity});

  @override
  _ScaleDetailScreenState createState() => _ScaleDetailScreenState();
}

class _ScaleDetailScreenState extends State<ScaleDetailScreen> {
  ScaletTab _activeTab = ScaletTab.DETAIL;
  PageController _controller = new PageController(keepPage: false);

  @override
  Widget build(BuildContext context) {
    var scale = widget.entity;
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        fixedColor: ThemeColors.ACCENT_DARK,
        currentIndex: _activeTab.index,
        onTap: navigateToPage,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
              backgroundColor: ThemeColors.ACCENT,
              icon: Icon(Icons.list),
              title: generateTitle(ScaletTab.DETAIL, 'Info')),
          BottomNavigationBarItem(
              icon: Icon(Icons.backup),
              title: generateTitle(ScaletTab.BACKUPS, 'Backups')),
        ],
      ),
      body: PageView(
        controller: _controller,
        physics: NeverScrollableScrollPhysics(),
        children: <Widget>[
          ScaleDetailPageView(entity: scale),
          BackupPage(scale),
        ],
      ),
    );
  }

  Widget generateTitle(ScaletTab tab, String title) {
    return Container(
        margin: const EdgeInsets.only(top: 4.0),
        child: Text(title,
            style: TextStyle(
                fontSize: 12.0,
                color: ThemeColors.ACCENT_DARK
                    .withOpacity(_activeTab == tab ? 1.0 : 0.3),
                fontWeight: FontWeight.w500)));
  }

  void navigateToPage(int index) {
    updateTab(ScaletTab.values[index]);
  }

  void updateTab(ScaletTab tab) {
    _controller.jumpToPage(tab.index);
    setState(() {
      _activeTab = tab;
    });
  }
}
