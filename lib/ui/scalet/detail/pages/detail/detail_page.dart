import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vscale_io/data/models/models.dart';
import 'package:vscale_io/di/injector.dart';
import 'package:vscale_io/domain/interactor/server_interactor.dart';
import 'package:vscale_io/ui/common/widget/app_bar/app_bar.dart';
import 'package:vscale_io/ui/common/widget/tag_widget.dart';
import 'package:vscale_io/ui/login/confirm_dialog.dart';
import 'package:vscale_io/ui/scalet/detail/component/header_widget.dart';
import 'package:vscale_io/ui/scalet/detail/component/location_widget.dart';
import 'package:vscale_io/ui/scalet/detail/component/scale_name_widget.dart';
import 'package:vscale_io/ui/scalet/detail/component/scale_rlan_detail_widget.dart';
import 'package:vscale_io/ui/scalet/detail/component/tags_widget.dart';
import 'package:vscale_io/ui/scalet/detail/dialog/change_rplan_dialog.dart';
import 'package:vscale_io/utils/utils.dart';

class ScaleDetailPageView extends StatefulWidget {
  ScaletEntity entity;

  ScaleDetailPageView({Key key, this.entity}) : super(key: key);

  @override
  _ScaleDetailPageViewState createState() => _ScaleDetailPageViewState();
}

class _ScaleDetailPageViewState extends State<ScaleDetailPageView>
    with AutomaticKeepAliveClientMixin {
  ScaletInteractor interactor = Injector.inject();

  @override
  void initState() {
    super.initState();
    _loadDetailInfoScalet(widget.entity.ctid);
  }

  @override
  Widget build(BuildContext context) {
    var scale = widget.entity;
    return Scaffold(
      appBar: VscaleAppBar(
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.edit),
            onPressed: () => _openRplanDialog(context, scale),
          ),
        ],
      ),
      body: Container(
        child: ListView(
          padding: EdgeInsets.all(8.0),
          shrinkWrap: true,
          children: <Widget>[
            ScaleNameWidget(scale),
            LocationWidget(scale.location),
            ScaleRplanDetailWidget(scale),
            prepareControlButtons(),
            prepareHostWidget(scale.hostname),
            TagsWidget(scale.tags),
            HeaderWidget("Public wired"),
            createItemValue(scale.publicAddress.address),
            createItemValue(scale.publicAddress.gateway),
            createItemValue(scale.publicAddress.netmask),
            HeaderWidget("SSH keys"),
            Wrap(
              children:
                  scale.keys.map((key) => TagWidget(Text(key.name))).toList(),
            ),
          ],
        ),
      ),
    );
  }

  void _openRplanDialog(context, rplan) {
    showDialog(
        context: context,
        builder: (context) {
          return ChangeRplanDialog(
            scale: rplan,
          );
        });
  }

  Widget createItemValue(String text) {
    return Text(
      text,
      style: const TextStyle(fontSize: 16.0),
    );
  }

  Column prepareHostWidget(String host) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        HeaderWidget("Host name"),
        createItemValue(host),
      ],
    );
  }

  Widget prepareControlButtons() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        RaisedButton(
          onPressed: () {},
          child: Text(
            "STOP",
            style: TextStyle(color: Colors.white),
          ),
        ),
        RaisedButton(
          onPressed: () {},
          child: Text("REBOOT", style: TextStyle(color: Colors.white)),
        ),
        RaisedButton(
          onPressed: removeScale,
          child: Text(
            "DELETE",
            style: TextStyle(color: Colors.white),
          ),
        ),
      ],
    );
  }

  void _loadDetailInfoScalet(String ctid) {
    interactor.getScaleInfo(ctid).then((info) {
      setState(() {
        widget.entity = info;
      });
    });
  }

  @override
  bool get wantKeepAlive => true;

  void removeScale() {
    var callback = (bool isRemove) {
      if (isRemove) {
        interactor.removeScale(widget.entity.ctid).then((res) {
          Navigator.pop(context);
        }).catchError((e) {
          Utils.showToast("Error $e");
        });
      }
    };
    showDialog(
        context: context,
        builder: (context) => ConfirmDialog(
            title: "Remove ${widget.entity.name}?", callback: callback));
  }
}
