import 'package:flutter/material.dart';
import 'package:vscale_io/data/models/scarlet.dart';
import 'package:vscale_io/ui/backup/view/backup_view_list.dart';
import 'package:vscale_io/ui/common/widget/app_bar/app_bar.dart';

class BackupPage extends StatefulWidget {
  ScaletEntity scale;

  BackupPage(this.scale);

  @override
  _BackupPageState createState() => _BackupPageState();
}

class _BackupPageState extends State<BackupPage>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: VscaleAppBar(),
        body: BackupListView(
          ctid: widget.scale.ctid,
        ));
  }

  @override
  bool get wantKeepAlive => true;
}
