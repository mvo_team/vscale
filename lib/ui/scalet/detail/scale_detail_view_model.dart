import 'package:vscale_io/domain/models/scale_detail_model.dart';
import 'package:vscale_io/domain/models/status_view_model.dart';
import 'package:vscale_io/ui/common/vm/base_view_model.dart';

class ScaleDetailViewModel extends BaseViewModel<ScaleDetailModel> {
  ScaleDetailViewModel({status, data, errorMessage})
      : super(status: status, data: data, errorMessage: errorMessage);

  factory ScaleDetailViewModel.init() {
    return BaseViewModel.init();
  }

  factory ScaleDetailViewModel.error(String errorMessage) {
    return BaseViewModel.error(errorMessage);
  }

  @override
  ScaleDetailViewModel success(ScaleDetailModel data) {
    return BaseViewModel.success(data);
  }
}
