import 'package:flutter/material.dart';
import 'package:vscale_io/data/models/rplan_entity.dart';
import 'package:vscale_io/di/injector.dart';
import 'package:vscale_io/domain/interactor/info_interactor.dart';
import 'package:vscale_io/utils/styles.dart';

class RplanWidget extends StatefulWidget {
  String plan;

  RplanWidget(this.plan);

  @override
  _RplanWidgetState createState() => _RplanWidgetState();
}

class _RplanWidgetState extends State<RplanWidget> {
  InfoInteractor interactor = Injector.inject();

  RplanEntity plan;

  @override
  void initState() {
    super.initState();

    print("Plan: ${widget.plan}");
    loadInfoByPlan(widget.plan);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: (plan != null)
          ? buildRplans()
          : Padding(
              padding: const EdgeInsets.all(8.0),
              child: SizedBox(
                width: MediaQuery.of(context).size.width - 96,
                height: 2.0,
                child: LinearProgressIndicator(),
              ),
            ),
    );
  }

  Widget buildRplans() {
    return Wrap(
      spacing: 8.0,
      children: <Widget>[
        _prepareElement("${plan.cpus} CPU"),
        _prepareElement("${(plan.disk / 1024).round()} GB SSD"),
        _prepareElement("${plan.memory} MB RAM"),
      ],
    );
  }

  Widget _prepareElement(String text) {
    return Padding(
      padding: const EdgeInsets.only(top: 4.0),
      child: Text(
        text,
        style: TextStyle(color: ThemeColors.HINT_COLOR),
      ),
    );
  }

  @protected
  void loadInfoByPlan(String plan) {
    interactor.getRplanByName(plan).then((item) {
      setState(() {
        this.plan = item;
      });
    });
  }
}
