import 'package:flutter/material.dart';
import 'package:vscale_io/data/models/models.dart';
import 'package:vscale_io/di/injector.dart';
import 'package:vscale_io/domain/interactor/info_interactor.dart';
import 'package:vscale_io/utils/styles.dart';

class LocationWidget extends StatefulWidget {
  String location;

  LocationWidget(this.location);

  @override
  _LocationWidgetState createState() => _LocationWidgetState();
}

class _LocationWidgetState extends State<LocationWidget> {
  InfoInteractor interactor = Injector.inject();
  LocationEntity entity;

  @override
  void initState() {
    super.initState();

    loadLocation(widget.location);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: (entity != null)
          ? Text(
              entity.description,
              style: TextStyle(color: ThemeColors.HINT_COLOR),
            )
          : Padding(
              padding: const EdgeInsets.all(8.0),
              child: SizedBox(
                width: MediaQuery.of(context).size.width - 96,
                height: 2.0,
                child: LinearProgressIndicator(),
              ),
            ),
    );
  }

  @protected
  void loadLocation(String location) {
    interactor.getLocationById(location).then((item) {
      setState(() {
        this.entity = item;
      });
    });
  }
}
