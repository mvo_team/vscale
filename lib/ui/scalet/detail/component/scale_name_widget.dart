import 'package:flutter/material.dart';
import 'package:vscale_io/data/models/models.dart';
import 'package:vscale_io/data/models/scale_status.dart';

class ScaleNameWidget extends StatelessWidget {
  ScaletEntity scale;


  ScaleNameWidget(@required this.scale);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Icon(Icons.power_settings_new,
            color: Status.STARTED != scale.status.status
                ? Colors.red
                : Colors.green),
        Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                scale.name,
                style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
              ),
            )),
      ],
    );
  }
}
