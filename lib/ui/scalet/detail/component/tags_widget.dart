import 'package:flutter/material.dart';
import 'package:vscale_io/data/models/models.dart';
import 'package:vscale_io/ui/common/widget/tag_widget.dart';
import 'package:vscale_io/ui/scalet/detail/component/header_widget.dart';

class TagsWidget extends StatelessWidget {
  List<TagEntity> tags;

  TagsWidget(this.tags);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        HeaderWidget("Tags"),
        (tags != null && tags.isNotEmpty)
            ? Wrap(
                alignment: WrapAlignment.start,
                direction: Axis.horizontal,
                children: tags
                    .map((tag) => TagWidget(Text(tag.name,
                        style: const TextStyle(fontWeight: FontWeight.bold))))
                    .toList()
                      ..add(buildAddTag()))
            : buildAddTag(),
      ],
    );
  }

  TagWidget buildAddTag() {
    return TagWidget(
      Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
        Icon(
          Icons.add,
          size: 16.0,
        ),
        Text(
          "Add tag",
          style: const TextStyle(fontWeight: FontWeight.bold),
        ),
      ]),
      callback: () {},
    );
  }
}
