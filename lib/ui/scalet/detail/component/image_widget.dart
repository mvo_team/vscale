import 'package:flutter/material.dart';
import 'package:vscale_io/data/models/models.dart';
import 'package:vscale_io/di/injector.dart';
import 'package:vscale_io/domain/interactor/info_interactor.dart';

class ImageWidget extends StatefulWidget {
  String image;

  ImageWidget(this.image);

  @override
  _ImageWidgetState createState() => _ImageWidgetState();
}

class _ImageWidgetState extends State<ImageWidget> {
  InfoInteractor interactor = Injector.inject();

  ImageEntity image;

  @override
  void initState() {
    super.initState();
    loadImage(widget.image);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: (image != null)
          ? buildImage()
          : Padding(
            padding: const EdgeInsets.all(8.0),
            child: SizedBox(
                width: MediaQuery.of(context).size.width - 96,
                height: 2.0,
                child: LinearProgressIndicator(),
              ),
          ),
    );
  }

  @protected
  void loadImage(String id) {
    interactor.getImageById(id).then((item) {
      setState(() {
        image = item;
      });
    });
  }

  Widget buildImage() {
    return Text(
      image.description,
      style: TextStyle(fontSize: 16.0),
    );
  }
}
