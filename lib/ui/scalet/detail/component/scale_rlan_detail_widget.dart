import 'package:flutter/material.dart';
import 'package:vscale_io/data/models/models.dart';
import 'package:vscale_io/ui/scalet/detail/component/image_widget.dart';
import 'package:vscale_io/ui/scalet/detail/component/rplan_widget.dart';

class ScaleRplanDetailWidget extends StatelessWidget {
  ScaletEntity scale;

  ScaleRplanDetailWidget(this.scale);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 8.0, top: 8.0, bottom: 8.0),
            child: Icon(Icons.data_usage),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                scale.rplan.toUpperCase(),
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              ImageWidget(scale.madeFrom),
              RplanWidget(scale.rplan),
            ],
          ),
        ],
      ),
    );
  }
}
