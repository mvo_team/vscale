import 'package:flutter/material.dart';
import 'package:vscale_io/data/models/models.dart';
import 'package:vscale_io/di/injector.dart';
import 'package:vscale_io/domain/interactor/info_interactor.dart';
import 'package:vscale_io/domain/interactor/server_interactor.dart';
import 'package:vscale_io/domain/models/rplan_tarif_model.dart';
import 'package:vscale_io/ui/common/vm/base_state.dart';
import 'package:vscale_io/ui/scalet/detail/dialog/dot_indicator.dart';
import 'package:vscale_io/utils/styles.dart';

class ChangeRplanDialog extends StatefulWidget {
  final ScaletEntity scale;

  const ChangeRplanDialog({Key key, this.scale}) : super(key: key);

  @override
  _ChangeRplanDialogState createState() => _ChangeRplanDialogState();
}

class _ChangeRplanDialogState extends BaseState<ChangeRplanDialog> {
  InfoInteractor interactor = Injector.inject();
  ScaletInteractor scaletInteractor = Injector.inject();

  List<RplanTarifModel> values;

  PageController _controller =
      PageController(viewportFraction: 0.85, initialPage: 0, keepPage: true);

  @override
  void initState() {
    super.initState();
    loadPlans();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: EdgeInsets.all(0.0),
      title: Text("Change plan"),
      actions: <Widget>[
        FlatButton(
          onPressed: () => _saveConfiguration(
              widget.scale.ctid, values[_controller.page.toInt()]),
          child: Text(
            "SAVE",
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        FlatButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text(
            "CANCEL",
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ],
      content: SizedBox(
        width: 320.0,
        height: 180.0,
        child: (values != null && values.isNotEmpty)
            ? Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: DotsIndicator(
                      controller: _controller,
                      itemCount: values.length,
                      onPageSelected: (i) {},
                      color: ThemeColors.ACCENT,
                    ),
                  ),
                  SizedBox(
                    height: 135.0,
                    child: PageView.builder(
                        itemCount: values.length,
                        controller: _controller,
                        physics: AlwaysScrollableScrollPhysics(),
                        itemBuilder: (context, i) {
                          return buildContainer(widget.scale, values[i]);
                        }),
                  )
                ],
              )
            : Center(
                child: CircularProgressIndicator(),
              ),
      ),
    );
  }

  Widget buildItem(title, value) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: <Widget>[
          Text(
            title,
            style: buildTextStyle(),
          ),
          Text(
            value,
            style: buildTextStyle(),
          ),
        ],
      ),
    );
  }

  Container buildContainer(ScaletEntity entity, RplanTarifModel item) {
    return Container(
      margin: EdgeInsets.all(8.0),
      decoration: BoxDecoration(border: Border.all(color: ThemeColors.ACCENT)),
      child: Stack(
        children: <Widget>[
          (entity.rplan == item.rplanEntity.id)
              ? buildCurrentPlan()
              : Container(),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                "${item.priceItem.month / 100} \u20bd",
                style: TextStyle(fontSize: 24.0, fontWeight: FontWeight.w500),
              ),
              Text("${item.priceItem.hour / 100} \u20bd in hour"),
              Divider(
                color: ThemeColors.ACCENT,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  buildItem("MEMORY", "${item.rplanEntity.memory}"),
                  buildItem("DISK", "${item.rplanEntity.disk / 1024} GB"),
                  buildItem("CORE", "${item.rplanEntity.cpus}"),
                ],
              )
            ],
          ),
        ],
      ),
    );
  }

  Positioned buildCurrentPlan() {
    return Positioned(
        top: 0.0,
        right: 0.0,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Icon(
            Icons.check,
            color: ThemeColors.ACCENT_DARK,
          ),
        ));
  }

  TextStyle buildTextStyle() => const TextStyle(fontSize: 12.0);

  void loadPlans() {
    interactor.getRplansInfo().then((values) {
      setState(() {
        this.values = values;
      });
    });

    interactor.getRplans().then((values) {});
  }

  _saveConfiguration(String ctid, RplanTarifModel item) {
    scaletInteractor
        .changeConfiguration(ctid, item.rplanEntity.id)
        .then((result) {
      Navigator.pop(context, result);
    }).catchError((e) {
      showError(message: e.toString());
    });
  }
}
