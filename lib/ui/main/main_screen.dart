import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:vscale_io/ui/account/account_screen.dart';
import 'package:vscale_io/ui/backup/backup_screen.dart';
import 'package:vscale_io/ui/main/app_tab.dart';
import 'package:vscale_io/ui/scalet/scalets_screen.dart';
import 'package:vscale_io/utils/styles.dart';

class MainScreen extends StatefulWidget {
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  PageController _controller = new PageController(keepPage: false);
  AppTab _activeTab = AppTab.SCALET;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        fixedColor: ThemeColors.ACCENT_DARK,
        currentIndex: _activeTab.index,
        onTap: navigateToPage,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
              backgroundColor: ThemeColors.ACCENT,
              icon: Icon(Icons.list),
              title: generateTitle(AppTab.SCALET, 'Servers')),
          BottomNavigationBarItem(
              icon: Icon(Icons.backup),
              title: generateTitle(AppTab.BACKUP, 'Backup')),
          BottomNavigationBarItem(
              icon: Icon(Icons.person_pin),
              title: generateTitle(AppTab.ACCOUNT, 'Account')),
        ],
      ),
      body: PageView(
        physics: NeverScrollableScrollPhysics(),
        controller: _controller,
        children: <Widget>[
          ScaletsScreen(),
          BackupScreen(),
          AccountScreen(),
        ],
      ),
    );
  }

  Widget generateTitle(AppTab tab, String title) {
    return Container(
        margin: const EdgeInsets.only(top: 4.0),
        child: Text(title,
            style: TextStyle(
                fontSize: 12.0,
                color: ThemeColors.ACCENT_DARK
                    .withOpacity(_activeTab == tab ? 1.0 : 0.3),
                fontWeight: FontWeight.w500)));
  }

  void navigateToPage(int index) {
    updateTab(AppTab.values[index]);
  }

  void updateTab(AppTab tab) {
    _controller.jumpToPage(tab.index);
    setState(() {
      _activeTab = tab;
    });
  }
}
