import 'package:flutter/material.dart';
import 'package:vscale_io/utils/styles.dart';

class EmptyBackupCard extends StatelessWidget {

  final VoidCallback callback;

  const EmptyBackupCard({Key key, this.callback}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(8.0),
      decoration: BoxDecoration(
          border:
              Border.all(style: BorderStyle.solid, color: ThemeColors.BORDER),
          borderRadius: BorderRadius.circular(8.0)),
      height: 150.0,
      constraints: BoxConstraints(maxHeight: 150.0),
      child: InkWell(
        onTap: () => callback(),
        child: Padding(
          padding:
              EdgeInsets.only(top: 8.0, bottom: 8.0, left: 16.0, right: 8.0),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Center(child: Icon(Icons.add)),
              Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: Text("Added new backup",
                    style: TextStyle(
                        fontSize: 14.0,
                        color: ThemeColors.HINT_COLOR,
                        fontFamily: "Open Sans,sans-serif")),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
