import 'package:vscale_io/data/models/models.dart';
import 'package:vscale_io/domain/models/status_view_model.dart';
import 'package:vscale_io/ui/common/vm/base_view_model.dart';

class BackupListViewModel extends BaseViewModel<List<BackupEntity>> {
  List<BackupEntity> data;
  ViewModelStatus status;
  String errorMessage;

  BackupListViewModel({this.status, this.data, this.errorMessage}):super();

  initState() {
    return BackupListViewModel(status: ViewModelStatus.LOADING);
  }

  success(List<BackupEntity> list) {
    return BackupListViewModel(status: ViewModelStatus.SUCCESS, data: list);
  }

  error(String errorMessage) {
    return BackupListViewModel(
        status: ViewModelStatus.ERROR, errorMessage: errorMessage);
  }
}
