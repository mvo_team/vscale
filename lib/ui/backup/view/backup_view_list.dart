import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vscale_io/data/models/models.dart';
import 'package:vscale_io/di/injector.dart';
import 'package:vscale_io/domain/interactor/index.dart';
import 'package:vscale_io/domain/models/status_view_model.dart';
import 'package:vscale_io/ui/backup/dialog/create_backup_dialog.dart';
import 'package:vscale_io/ui/backup/view/backup_list_view_model.dart';
import 'package:vscale_io/ui/backup/view/widget/empty_backup_card.dart';
import 'package:vscale_io/ui/common/widget/backup_card.dart';

class BackupListView extends StatefulWidget {
  String ctid;

  BackupListView({this.ctid});

  @override
  _BackupListViewState createState() => _BackupListViewState();
}

class _BackupListViewState extends State<BackupListView> {
  BackupListViewModel vm = BackupListViewModel().initState();
  BackupInteractor _interactor = Injector.inject<BackupInteractor>();

  @override
  void initState() {
    super.initState();
    _loadScalets(widget.ctid);
  }

  @override
  Widget build(BuildContext context) {
    return prepareScreen();
  }

  Widget prepareScreen() {
    switch (vm.status) {
      case ViewModelStatus.SUCCESS:
        return prepareSuccessWidget(vm.data);
        break;
      case ViewModelStatus.ERROR:
        return Center(
          child: Text(vm.errorMessage),
        );
        break;
      case ViewModelStatus.LOADING:
        return Center(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: CircularProgressIndicator(),
          ),
        );
        break;
    }
  }

  Widget prepareSuccessWidget(List<BackupEntity> items) {
    if (items != null && items.isNotEmpty) {
      return RefreshIndicator(
        onRefresh: _handleRefresh,
        notificationPredicate: (n) => vm.status != ViewModelStatus.LOADING,
        child: ListView.builder(
          itemCount: items.length,
          itemBuilder: (BuildContext context, int pos) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: BackupCard(entity: items[pos]),
            );
          },
        ),
      );
    } else {
      return ListView(children: [
        EmptyBackupCard(
          callback: _createBackup,
        ),
      ]);
    }
  }

  Future<Null> _handleRefresh() {
    return getMethod(widget.ctid)
        .then((list) => vm.success(list))
        .catchError((e) => vm.error("error loadScarlet $e"))
        .then((state) {
      setState(() {
        vm = state;
      });
    }).then((r) => null);
  }

  void _loadScalets(String ctid) {
    setState(() {
      this.vm = vm.initState();
    });
    getMethod(ctid)
        .then((list) => vm.success(list))
        .catchError((e) => vm.error("error loadScarlet $e"))
        .then((state) {
      setState(() {
        vm = state;
      });
    });
  }

  Future<List<BackupEntity>> getMethod(String ctid) {
    if (ctid != null && ctid.isNotEmpty) {
      return _interactor.getListBackupByScale(ctid);
    } else {
      return _interactor.getListBackup();
    }
  }

  void _createBackup() {
    showDialog(
        context: context,
        builder: (context) => CreateBackupDialog(
              ctid: widget.ctid,
            )).then((result) {
      if (result != null) {
        var newVm = vm.success(vm.data..add(result));
        setState(() {
          vm = newVm;
        });
      } else {}
    });
  }
}
