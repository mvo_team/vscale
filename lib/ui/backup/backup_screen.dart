import 'package:flutter/material.dart';
import 'package:vscale_io/ui/backup/view/backup_view_list.dart';
import 'package:vscale_io/ui/common/widget/app_bar/app_bar.dart';

class BackupScreen extends StatefulWidget {
  _BackupScreenState createState() => _BackupScreenState();
}

class _BackupScreenState extends State<BackupScreen>
    with AutomaticKeepAliveClientMixin {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: VscaleAppBar(),
      body: BackupListView(),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
