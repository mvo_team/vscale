import 'package:flutter/material.dart';
import 'package:vscale_io/di/injector.dart';
import 'package:vscale_io/domain/interactor/index.dart';
import 'package:vscale_io/utils/styles.dart';

class CreateBackupDialog extends StatefulWidget {
  final String ctid;

  const CreateBackupDialog({Key key, this.ctid}) : super(key: key);

  @override
  _CreateBackupDialogState createState() => _CreateBackupDialogState();
}

class _CreateBackupDialogState extends State<CreateBackupDialog> {
  final nameController = TextEditingController();
  bool _errorName = false;

  BackupInteractor interactor = Injector.inject();

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: EdgeInsets.only(left: 16.0, right: 16.0, top: 16.0),
      title: Text("Create backup"),
      actions: <Widget>[
        FlatButton(
          onPressed: () => _createBackup(widget.ctid),
          child: Text(
            "CREATE",
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        FlatButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text(
            "CANCEL",
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ],
      content: SizedBox(
        width: 320.0,
        height: 160.0,
        child: ListView(
          children: <Widget>[
            TextField(
              controller: nameController,
              decoration: InputDecoration(
                errorText: _errorName ? "Not empty" : null,
                border: UnderlineInputBorder(
                    borderSide: BorderSide(color: ThemeColors.BORDER)),
                enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: ThemeColors.BORDER)),
                focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: ThemeColors.BORDER)),
                contentPadding: EdgeInsets.all(4.0),
                hintText: "Name",
              ),
            ),
          ],
        ),
      ),
    );
  }

  _createBackup(String ctid) {
    if (nameController.text.isNotEmpty) {
      interactor.createBackup(ctid, nameController.text).then((backup) {
        Navigator.pop(context, backup);
      });
    } else {}
  }
}
