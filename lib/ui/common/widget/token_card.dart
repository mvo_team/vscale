import 'package:flutter/material.dart';
import 'package:vscale_io/data/models/user_token_entity.dart';
import 'package:vscale_io/utils/styles.dart';

class TokenCard extends StatefulWidget {
  UserTokenEntity _token;
  Function(UserTokenEntity token) _removeCallback;
  Function(UserTokenEntity token) _selectCallback;

  TokenCard(
      {UserTokenEntity token,
      Function(UserTokenEntity token) selectCallback,
      Function(UserTokenEntity token) removeCallback}) {
    this._token = token;
    this._selectCallback = selectCallback;
    this._removeCallback = removeCallback;
  }
  _TokenCardState createState() => _TokenCardState();
}

class _TokenCardState extends State<TokenCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(8.0),
      decoration: BoxDecoration(
          border:
              Border.all(style: BorderStyle.solid, color: ThemeColors.BORDER),
          borderRadius: BorderRadius.circular(0.0)),
      child: InkWell(
        onTap: () => widget._selectCallback(widget._token),
        child: Padding(
          padding:
              EdgeInsets.only(top: 8.0, bottom: 8.0, left: 16.0, right: 8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      widget._token.name,
                      style: TextStyle(
                          fontSize: 16.0,
                          color: ThemeColors.TEXT_COLOR,
                          fontFamily: "Open Sans,sans-serif"),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text(widget._token.token,
                          style: TextStyle(
                              fontSize: 14.0,
                              color: ThemeColors.HINT_COLOR,
                              fontFamily: "Open Sans,sans-serif")),
                    ),
                  ],
                ),
              ),
              InkWell(
                onTap: () => widget._removeCallback(widget._token),
                child: Container(
                  padding: EdgeInsets.all(8.0),
                  child: Column(
                    children: <Widget>[
                      Icon(
                        Icons.delete,
                        color: ThemeColors.ICON,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
