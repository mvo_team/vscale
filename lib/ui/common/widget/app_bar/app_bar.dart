import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class VscaleAppBar extends StatelessWidget implements PreferredSizeWidget {
  List<Widget> actions;

  PreferredSizeWidget appBar;

  String title;

  VscaleAppBar({this.title, this.actions}) {
    appBar = AppBar(
      title: title != null
          ? Text(title)
          : Container(
              child: SvgPicture.asset('assets/images/vscale.svg'),
            ),
      actions: actions,
    );
  }

  @override
  Widget build(BuildContext context) {
    return appBar;
  }

  @override
  Size get preferredSize => appBar.preferredSize;
}
