import 'package:flutter/material.dart';
import 'package:vscale_io/data/models/user_token_entity.dart';
import 'package:vscale_io/ui/token/add_token_dialog.dart';
import 'package:vscale_io/utils/styles.dart';

class TokenEmptyCard extends StatefulWidget {
  Function(UserTokenEntity token) _callback;

  TokenEmptyCard(Function(UserTokenEntity token) callack) {
    this._callback = callack;
  }

  _TokenEmptyCardState createState() => _TokenEmptyCardState();
}

class _TokenEmptyCardState extends State<TokenEmptyCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(8.0),
      decoration: BoxDecoration(
          border:
              Border.all(style: BorderStyle.solid, color: ThemeColors.BORDER),
          borderRadius: BorderRadius.circular(8.0)),
      height: 150.0,
      constraints: BoxConstraints(maxHeight: 150.0),
      child: InkWell(
        borderRadius: BorderRadius.circular(4.0),
        onTap: () => showTokenDialog(context, widget._callback),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Center(child: Icon(Icons.add)),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Text("Added new token",
                      style: TextStyle(
                          fontSize: 14.0,
                          color: ThemeColors.HINT_COLOR,
                          fontFamily: "Open Sans,sans-serif")),
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(top: 8.0),
              child: Text("Generate new token on vscale site",
                  style: TextStyle(
                      fontSize: 12.0,
                      color: ThemeColors.HINT_COLOR,
                      fontFamily: "Open Sans,sans-serif")),
            ),
            FlatButton(
              onPressed: () => print("www.vscale.io"),
              child: Text("www.vscale.io",
                  style: TextStyle(
                      fontSize: 14.0,
                      color: ThemeColors.ACCENT,
                      fontFamily: "Open Sans,sans-serif")),
            )
          ],
        ),
      ),
    );
  }

  
}

abstract class ClickListener<T> {
   void onClick(T value);
}
