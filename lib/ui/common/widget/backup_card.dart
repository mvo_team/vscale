import 'package:flutter/material.dart';
import 'package:vscale_io/data/models/backup_status.dart';
import 'package:vscale_io/data/models/models.dart';
import 'package:vscale_io/utils/styles.dart';

class BackupCard extends StatefulWidget {
  BackupEntity _entity;

  BackupCard({BackupEntity entity}) {
    this._entity = entity;
  }

  _BackupCardState createState() => _BackupCardState();
}

class _BackupCardState extends State<BackupCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 4.0, horizontal: 8.0),
      decoration: BoxDecoration(
          border:
              Border.all(style: BorderStyle.solid, color: ThemeColors.BORDER),
          borderRadius: BorderRadius.circular(0.0)),
      child: InkWell(
        onTap: () => {},
        child: Padding(
          padding:
              EdgeInsets.only(top: 8.0, bottom: 8.0, left: 16.0, right: 8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: generateStatusWidget(widget._entity),
                  ),
                  PopupMenuButton(
                    icon: Icon(Icons.settings, color: ThemeColors.ICON),
                    itemBuilder: (context) {
                      return [
                        PopupMenuItem(
                          child: ListTile(
                          leading: Icon(Icons.stop, color: ThemeColors.ICON),
                          title: Text("Stop"),
                        ),)
                      ];
                    },
                  ),
                ],
              ),
              Text(
                widget._entity.name,
                style: TextStyle(
                    fontSize: 18.0,
                    color: ThemeColors.TEXT_COLOR,
                    fontFamily: "Open Sans,sans-serif",
                    fontWeight: FontWeight.bold),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Row(
                  children: <Widget>[
                    Text("created:"),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 4.0, horizontal: 8.0),
                        child: Text(widget._entity.created),
                      ),
                    ),
                  ],
                ),
              ),
              Row(
                children: <Widget>[
                  Text("size:"),
                  Expanded(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 4.0, horizontal: 8.0),
                        child: Text("${widget._entity.size.toString()} Gb"),
                      )),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget generateStatusWidget(BackupEntity entity) {
    return Row(
      children: <Widget>[
        generateIconStatus(entity.isActive),
        generateTitleStatus(entity.isActive),
      ],
    );
  }

  Widget generateIconStatus(bool isActive) {
    return Icon(
      Icons.cloud,
      color: isActive ? Colors.green : Colors.red,
    );
  }

  Widget generateTitleStatus(bool isActive) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(
        isActive ? "active" : "disable",
        style: TextStyle(color: isActive ? Colors.green : Colors.red, fontWeight: FontWeight.bold),
      ),
    );
  }
}
