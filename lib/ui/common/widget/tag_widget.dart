import 'package:flutter/material.dart';
import 'package:vscale_io/utils/styles.dart';

class TagWidget extends StatelessWidget {
  Widget widget;

  VoidCallback callback;

  TagWidget(this.widget, {VoidCallback this.callback});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 8.0, top: 8.0),
      child: Material(
        color: ThemeColors.BACKGROUD_TAG_COLOR,
        child: InkWell(
          onTap: this.callback,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: widget,
          ),
        ),
      ),
    );
  }
}
