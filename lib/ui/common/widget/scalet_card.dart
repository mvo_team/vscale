import 'package:flutter/material.dart';
import 'package:vscale_io/data/models/models.dart';
import 'package:vscale_io/data/models/scale_status.dart';
import 'package:vscale_io/ui/common/widget/tag_widget.dart';
import 'package:vscale_io/utils/styles.dart';

class ScaletCard extends StatefulWidget {
  ScaletEntity scalet;

  @protected
  VoidCallback powerCallback;

  @protected
  VoidCallback rebootCallback;

  @protected
  VoidCallback onTap;

  ScaletCard(
      {this.scalet, this.powerCallback, this.rebootCallback, this.onTap});

  _ScaletCardState createState() => _ScaletCardState();
}

class _ScaletCardState extends State<ScaletCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 4.0, horizontal: 8.0),
      decoration: BoxDecoration(
          border:
              Border.all(style: BorderStyle.solid, color: ThemeColors.BORDER),
          borderRadius: BorderRadius.circular(0.0)),
      child: InkWell(
        onTap: () => widget.onTap(),
        child: Padding(
          padding:
              EdgeInsets.only(top: 8.0, bottom: 8.0, left: 16.0, right: 8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: generateStatusWidget(widget.scalet.status),
                  ),
                  PopupMenuButton(
                    icon: Icon(Icons.settings, color: ThemeColors.ICON),
                    onSelected: _selectItem,
                    itemBuilder: (context) {
                      var status = widget.scalet.status.status;
                      return [
                        PopupMenuItem(
                          value: 0,
                          child: ListTile(
                            leading: Icon(Icons.power_settings_new,
                                color: Status.STARTED == status
                                    ? Colors.red
                                    : Colors.green),
                            title: Status.STARTED == status
                                ? Text("Power off")
                                : Text("Power on"),
                          ),
                        ),
                        PopupMenuItem(
                          value: 1,
                          child: ListTile(
                            leading:
                                Icon(Icons.refresh, color: ThemeColors.ICON),
                            title: Text("Reboot"),
                          ),
                        )
                      ];
                    },
                  ),
                ],
              ),
              Text(
                widget.scalet.name,
                style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                child: Text(
                    widget.scalet.publicAddress.address ?? "empty address"),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Icon(Icons.sentiment_very_satisfied),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(widget.scalet.madeFrom),
                  ),
                ],
              ),
              Wrap(
                children: _prepareTags(widget.scalet.tags),
              )
            ],
          ),
        ),
      ),
    );
  }

  List<Widget> _prepareTags(List<TagEntity> list) {
    return list.map((item) => TagWidget(Text(item.name))).toList();
  }

  Widget generateStatusWidget(ScaletStatusEntity status) {
    return Row(
      children: <Widget>[
        generateIconStatus(status),
        generateTitleStatus(status),
      ],
    );
  }

  Widget generateIconStatus(ScaletStatusEntity status) {
    return Icon(
      Icons.power_settings_new,
      color: status.color,
    );
  }

  Widget generateTitleStatus(ScaletStatusEntity status) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(
        status.title,
        style: TextStyle(color: status.color, fontWeight: FontWeight.bold),
      ),
    );
  }

  void _selectItem(int value) {
    switch (value) {
      case 0:
        widget.powerCallback();
        break;
      case 1:
        widget.rebootCallback();
        break;
    }
  }
}
