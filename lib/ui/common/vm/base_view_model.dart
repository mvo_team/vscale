import 'package:vscale_io/domain/models/status_view_model.dart';

class BaseViewModel<TYPE> {
  TYPE data;
  ViewModelStatus status;
  String errorMessage;

  BaseViewModel({this.status, this.data, this.errorMessage});

  factory BaseViewModel.init() {
    return BaseViewModel(status: ViewModelStatus.LOADING);
  }

  factory BaseViewModel.success(TYPE data) {
    return BaseViewModel(status: ViewModelStatus.SUCCESS, data: data);
  }
  factory BaseViewModel.error(String errorMessage) {
    return BaseViewModel(status:ViewModelStatus.ERROR ,errorMessage: errorMessage);
  }
}
