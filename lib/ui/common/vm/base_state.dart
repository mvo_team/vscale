import 'package:flutter/material.dart';
import 'package:vscale_io/utils/utils.dart';

abstract class BaseState<SW extends StatefulWidget> extends State<SW> {
  void showError({String message}) {
    Utils.showToast(
        message != null ? "Error occured: $message" : "Error operation");
  }
}
