import 'package:vscale_io/data/models/models.dart';
import 'package:vscale_io/domain/models/status_view_model.dart';
import 'package:vscale_io/ui/common/vm/base_view_model.dart';

class ImageViewModel extends BaseViewModel<List<ImageEntity>> {
  ImageViewModel({status, data, errorMessage})
      : super(status: status, data: data, errorMessage: errorMessage);

  factory ImageViewModel.init() {
    return ImageViewModel(status: ViewModelStatus.LOADING);
  }

  factory ImageViewModel.success(List<ImageEntity> list) {
    return ImageViewModel(status: ViewModelStatus.SUCCESS, data: list);
  }

  factory ImageViewModel.error(String errorMessage) {
    return ImageViewModel(
        status: ViewModelStatus.ERROR, errorMessage: errorMessage);
  }
}
