import 'package:flutter/material.dart';
import 'package:vscale_io/data/models/image_entity.dart';
import 'package:vscale_io/di/injector.dart';
import 'package:vscale_io/domain/interactor/info_interactor.dart';
import 'package:vscale_io/ui/common/widget/app_bar/app_bar.dart';
import 'package:vscale_io/ui/image/image_view_model.dart';
import 'package:vscale_io/utils/styles.dart';

class ImageDialog extends StatefulWidget {
  @override
  _ImageDialogState createState() => _ImageDialogState();
}

class _ImageDialogState extends State<ImageDialog> {
  PageController _controller = PageController();

  var vm = ImageViewModel.init();

  InfoInteractor _interactor = Injector.inject();

  @override
  void initState() {
    super.initState();
    _loadImages();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: VscaleAppBar(),
      body: Column(
        children: <Widget>[
          Expanded(
            child: GridView.builder(
                itemCount: vm.data.length,
                shrinkWrap: true,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                ),
                itemBuilder: (context, indx) => buildContainer(vm.data[indx])),
          )
        ],
      ),
    );
  }

  void _loadImages() {
    vm = ImageViewModel.init();
    _interactor.getImages().then((items) {
      setState(() {
        vm = ImageViewModel.success(items);
      });
    }).catchError((e) {
      setState(() {
        vm = ImageViewModel.error(e);
      });
    });
  }
}

Widget buildContainer(ImageEntity data) {
  return Container(
    margin: EdgeInsets.symmetric(vertical: 4.0, horizontal: 8.0),
    decoration: BoxDecoration(
        border: Border.all(style: BorderStyle.solid, color: ThemeColors.BORDER),
        borderRadius: BorderRadius.circular(0.0)),
    child: InkWell(
      onTap: () => {},
      child: Padding(
        padding: EdgeInsets.only(top: 8.0, bottom: 8.0, left: 16.0, right: 8.0),
        child: Column(
          children: <Widget>[
            Text(data.id),
            Text(data.description),
            Text("${data.size / 1024} Gb"),
          ],
        ),
      ),
    ),
  );
}
