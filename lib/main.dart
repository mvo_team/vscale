import 'package:flutter/material.dart';
import 'package:vscale_io/ui/splash/splash_screen.dart';
import 'package:vscale_io/utils/styles.dart';

void main() => runApp(VscaleApp());

class VscaleApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Vscale.io client',
      theme: VscaleTheme.appTheme,
      debugShowCheckedModeBanner: false,
      home: SplashScreen(),
    );
  }
}