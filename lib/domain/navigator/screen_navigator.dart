import 'package:flutter/material.dart';
import 'package:vscale_io/data/models/scarlet.dart';
import 'package:vscale_io/ui/login/login_screen.dart';
import 'package:vscale_io/ui/main/main_screen.dart';
import 'package:vscale_io/ui/scalet/add/add_scalet_dialog.dart';
import 'package:vscale_io/ui/scalet/detail/scale_detail_screen.dart';
import 'package:vscale_io/ui/settings/setting_screen.dart';
import 'package:vscale_io/utils/utils.dart';

class ScreenNavigator {
  static void navigateToLogin(BuildContext context) {
    Navigator.of(context)
        .pushReplacement(Utils.buildRouteNavigation(LoginScreen()));
  }

  static void navigateToMain(BuildContext context) {
    Navigator.of(context)
        .pushReplacement(Utils.buildRouteNavigation(MainScreen()));
  }

  static void navigateToSettings(BuildContext context) {
    Navigator.of(context).push(Utils.buildRouteNavigation(SettingScreen()));
  }

  static Future<Object> showAddScaletDialog(BuildContext context) {
    return Navigator.of(context).push(Utils.buildMaterialRoute(AddScaletDialog()));
  }

  static navigateToDetailScalet(BuildContext context, ScaletEntity item) {
    Navigator.of(context).push(Utils.buildRouteNavigation(ScaleDetailScreen(
      entity: item,
    )));
  }
}
