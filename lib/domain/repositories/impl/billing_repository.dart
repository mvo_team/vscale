import 'package:vscale_io/data/models/balance_entity.dart';
import 'package:vscale_io/data/models/prices_entity.dart';
import 'package:vscale_io/data/provider/network/network_provider.dart';
import 'package:vscale_io/data/provider/prefs/prefs_provider.dart';
import 'package:vscale_io/domain/repositories/billing_repository.dart';

class BillingRepositoryImpl implements BillingRepository {
  NetworkProvider _networkProvider;
  PrefsProvider _prefsProvider;

  BillingRepositoryImpl(NetworkProvider np, PrefsProvider pp) {
    this._networkProvider = np;
    this._prefsProvider = pp;
  }

  @override
  Future<BalanceEntity> getBalance() {
    return _networkProvider.getBalance();
  }

  @override
  Future<PricesEntity> getPrices() {
    return _networkProvider.getPrices();
  }
}
