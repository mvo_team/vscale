import 'package:meta/meta.dart';
import 'package:vscale_io/data/models/backup_entity.dart';
import 'package:vscale_io/data/provider/network/network_provider.dart';
import 'package:vscale_io/data/provider/prefs/prefs_provider.dart';
import 'package:vscale_io/domain/repositories/backup_repository.dart';
import 'package:vscale_io/domain/repositories/base/cached_repository.dart';

class BackupRepositoryImpl extends CachedRepository
    implements BackupRepository {
  NetworkProvider _networkProvider;
  PrefsProvider _prefsProvider;
  static const String _backups = "backups";

  BackupRepositoryImpl(NetworkProvider np, PrefsProvider pp) {
    this._networkProvider = np;
    this._prefsProvider = pp;
  }

  @override
  Future<List<BackupEntity>> getBackupList() {
    if (containCache(_backups)) {
      return Future.value(get(_backups));
    } else {
      return getRemoteBackup();
    }
  }

  @protected
  Future<List<BackupEntity>> getRemoteBackup() {
    return _networkProvider.getListBackup().then((value) {
      //put(_backups, value);
      return value;
    });
  }

  @override
  Future<List<BackupEntity>> getBackupListByScalet(String ctid) {
    return getBackupList().then((values) {
      return values.where((item) => item.scalet == ctid).toList();
    });
  }

  @override
  Future<BackupEntity> createBackup(String ctid, String name) {
    return _networkProvider.createBackup(ctid, name);
  }


}
