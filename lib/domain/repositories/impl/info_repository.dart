import 'package:vscale_io/data/models/models.dart';
import 'package:vscale_io/data/provider/network/network_provider.dart';
import 'package:vscale_io/domain/repositories/info_repository.dart';

class InfoRepositoryImpl extends InfoRepository {
  NetworkProvider np;

  List<LocationEntity> _cacheLocations;
  List<RplanEntity> _cacheRplans;
  List<ImageEntity> _cacheImages;

  InfoRepositoryImpl(this.np);

  @override
  Future<List<LocationEntity>> getLocations() {
    if (_cacheLocations == null || _cacheLocations.isEmpty) {
      return _loadRemoteLocations();
    } else {
      return Future.value(_cacheLocations);
    }
  }

  Future<List<LocationEntity>> _loadRemoteLocations() {
    return np.getLocations().then((items) {
      _cacheLocations = items;
      return items;
    });
  }

  @override
  Future<LocationEntity> getLocationById(String id) {
    return getLocations().then((items) {
      return items.firstWhere((item) => item.id == id, orElse: null);
    });
  }

  @override
  Future<List<RplanEntity>> getRplans() {
    if (_cacheRplans == null || _cacheRplans.isEmpty) {
      return _loadRemoteRplans();
    } else {
      return Future.value(_cacheRplans);
    }
  }

  Future<List<RplanEntity>> _loadRemoteRplans() {
    return np.getRplans().then((items) {
      _cacheRplans = items;
      return items;
    });
  }

  @override
  Future<RplanEntity> getRplanByName(String name) {
    return getRplans().then((items) {
      return items.firstWhere((item) => item.id == name, orElse: null);
    });
  }

  @override
  Future<List<ImageEntity>> getImages() {
    if (_cacheImages == null || _cacheImages.isEmpty) {
      return _loadRemoteImages();
    } else {
      return Future.value(_cacheImages);
    }
  }

  @override
  Future<ImageEntity> getImageById(String id) {
    return getImages().then((items) {
      return items.firstWhere((item) => item.id == id, orElse: null);
    });
  }

  Future<List<ImageEntity>> _loadRemoteImages() {
    return np.getImages().then((items) {
      _cacheImages = items;
      return items;
    });
  }
}
