import 'dart:async';

import 'package:vscale_io/data/models/account.dart';
import 'package:vscale_io/data/models/user_token_entity.dart';
import 'package:vscale_io/data/provider/network/network_provider.dart';
import 'package:vscale_io/data/provider/prefs/prefs_provider.dart';
import 'package:vscale_io/domain/repositories/account_repository.dart';

class AccountRepositoryImpl extends AccountRepository {
  NetworkProvider _networkProvider;
  PrefsProvider _prefsProvider;

  AccountRepositoryImpl(NetworkProvider np, PrefsProvider pp) {
    this._networkProvider = np;
    this._prefsProvider = pp;
  }

  @override
  Future<AccountEntity> getAccount() {
    return _networkProvider.getAccount();
  }

  @override
  Future<bool> setToken(UserTokenEntity token) {
    return _prefsProvider.setToken(token);
  }

  @override
  Future<bool> addToken(UserTokenEntity token) {
    return _prefsProvider.saveToken(token);
  }

  @override
  Future<bool> isLoginLastToken() {
    return _prefsProvider.isLoginLastToken();
  }

  @override
  Future<UserTokenEntity> getToken() {
    return _prefsProvider.getToken();
  }

  @override
  Future<List<UserTokenEntity>> getListToken() {
    return _prefsProvider.getUserToken();
  }

  @override
  Future<bool> isLogged() {
    return _prefsProvider.getToken().then((token) => token != null && token.token.isNotEmpty);
  }

  @override
  Future<bool> removeToken(UserTokenEntity token) {
    return _prefsProvider.removeToken(token);
  }
}
