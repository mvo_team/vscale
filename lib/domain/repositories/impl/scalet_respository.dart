import 'dart:async';

import 'package:vscale_io/data/models/scale_status.dart';
import 'package:vscale_io/data/models/scarlet.dart';
import 'package:vscale_io/data/provider/network/network_provider.dart';
import 'package:vscale_io/data/provider/prefs/prefs_provider.dart';
import 'package:vscale_io/domain/repositories/server_repository.dart';

class ScaletRepositoryImpl implements ScaletRepository {
  NetworkProvider _networkProvider;
  PrefsProvider _prefsProvider;

  ScaletRepositoryImpl(NetworkProvider np, PrefsProvider pp) {
    this._networkProvider = np;
    this._prefsProvider = pp;
  }

  @override
  Future<List<ScaletEntity>> getScaletList() {
    return _networkProvider.getScaletList();
  }

  @override
  Future<ScaletEntity> restartServer(String ctid) {
    return parseEntity(_networkProvider.restarstScalets(ctid));
  }

  @override
  Future<ScaletEntity> startServer(String ctid) {
    return parseEntity(_networkProvider.startScalet(ctid));
  }

  @override
  Future<ScaletEntity> stopServer(String ctid) {
    return parseEntity(_networkProvider.stopScalet(ctid));
  }

  parseEntity(Future<ScaletEntity> future) {
    return future.then((entity) {
      entity.status = ScaletStatusEntity.parseStatus(
          (entity.status.status == Status.STARTED) ? "stopped" : "started");
      return entity;
    });
  }

  @override
  Future<ScaletEntity> getScaleInfo(String ctid) {
    return _networkProvider.getDetailScalet(ctid);
  }


  @override
  Future<ScaletEntity> removeScale(String ctid) {
    return _networkProvider.removeScalet(ctid);
  }

  @override
  Future<ScaletEntity> changeConfiguration(String ctid, String rplan) {
    print("$ctid $rplan");
    return _networkProvider.upgradeScalet(ctid, rplan);
  }
}
