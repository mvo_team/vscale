import 'dart:async';

import 'package:vscale_io/data/models/account.dart';
import 'package:vscale_io/data/models/user_token_entity.dart';

abstract class AccountRepository {
  Future<AccountEntity> getAccount();
  Future<bool> setToken(UserTokenEntity token);
  Future<bool> addToken(UserTokenEntity token);
  Future<UserTokenEntity> getToken();
  Future<bool> isLogged();
  Future<bool> removeToken(UserTokenEntity token);
  Future<bool> isLoginLastToken();
  Future<List<UserTokenEntity>> getListToken();
}