import 'package:vscale_io/data/models/models.dart';

abstract class InfoRepository {
  Future<List<LocationEntity>> getLocations();

  Future<LocationEntity> getLocationById(String id);

  Future<List<RplanEntity>> getRplans();

  Future<RplanEntity> getRplanByName(String name);

  Future<List<ImageEntity>> getImages();

  Future<ImageEntity> getImageById(String id);
}
