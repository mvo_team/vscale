import 'dart:async';

import 'package:vscale_io/data/models/scarlet.dart';

abstract class ScaletRepository {
  Future<List<ScaletEntity>> getScaletList();

  Future<ScaletEntity> startServer(String ctid);

  Future<ScaletEntity> stopServer(String ctid);

  Future<ScaletEntity> restartServer(String ctid);

  Future<ScaletEntity> getScaleInfo(String ctid);

  Future<ScaletEntity> removeScale(String ctid);

  Future<ScaletEntity> changeConfiguration(String ctid, String id);
}
