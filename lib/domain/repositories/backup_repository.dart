import 'package:vscale_io/data/models/models.dart';

abstract class BackupRepository {
  Future<List<BackupEntity>> getBackupList();
  Future<List<BackupEntity>> getBackupListByScalet(String ctid);
  Future<BackupEntity> createBackup(String ctid, String name);
}