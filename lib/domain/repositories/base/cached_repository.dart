import 'package:meta/meta.dart';

abstract class CachedRepository {
  final Map<String, dynamic> _cache = Map();

  @protected
  void put(String key, dynamic value) {
    _cache[key] = value;
  }

  @protected
  dynamic get(String key) {
    return _cache[key];
  }

  @protected
  bool containCache(String key) {
    return _cache[key] != null;
  }
}
