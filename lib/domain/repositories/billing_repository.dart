import 'package:vscale_io/data/models/models.dart';

abstract class BillingRepository {

  Future<BalanceEntity> getBalance();

  Future<PricesEntity> getPrices();
}