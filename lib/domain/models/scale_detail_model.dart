import 'package:vscale_io/data/models/models.dart';

class ScaleDetailModel {
  AccountEntity accountEntity;
  BalanceEntity balanceEntity;
  List<SshKeyEntity> listKeys;

  ScaleDetailModel(this.accountEntity, this.balanceEntity, this.listKeys);
}
