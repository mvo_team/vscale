import 'package:vscale_io/data/models/models.dart';

class RplanTarifModel {
  final RplanEntity rplanEntity;
  final PriceItem priceItem;

  RplanTarifModel({this.rplanEntity, this.priceItem});
}
