import 'package:vscale_io/domain/repositories/backup_repository.dart';
import 'package:vscale_io/domain/repositories/server_repository.dart';
import 'dart:async';
import 'package:vscale_io/data/models/models.dart';

class BackupInteractor {
  BackupRepository _repository;

  BackupInteractor(BackupRepository rep) {
    this._repository = rep;
  }

  Future<List<BackupEntity>> getListBackup() {
    return _repository.getBackupList();
  }

  Future<List<BackupEntity>> getListBackupByScale(String ctid) {
    return _repository.getBackupListByScalet(ctid);
  }

  Future<BackupEntity> createBackup(String ctid, String name) {
    return _repository.createBackup(ctid, name);
  }
}