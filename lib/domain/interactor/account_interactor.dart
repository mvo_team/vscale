import 'dart:async';

import 'package:vscale_io/data/models/models.dart';
import 'package:vscale_io/data/models/user_token_entity.dart';
import 'package:vscale_io/domain/repositories/account_repository.dart';

class AccountInteractor {
  AccountRepository _repository;

  AccountInteractor(AccountRepository rep) {
    this._repository = rep;
  }

  Future<AccountEntity> getAccount() {
    return _repository.getAccount();
  }

  Future<bool> addToken(String name, String token) {
    return _repository.addToken(UserTokenEntity(name: name, token: token));
  }

  Future<bool> isLoginLastToken() {
    return _repository.isLoginLastToken();
  }

  Future<bool> selectToken(UserTokenEntity token) {
    return _repository.setToken(token);
  }

  Future<bool> isLogged() {
    return _repository.isLogged();
  }

  Future<bool> removeToken(UserTokenEntity token) {
    return _repository.removeToken(token);
  }

  Future<List<UserTokenEntity>> getListToken() {
    return _repository.getListToken();
  }
}
