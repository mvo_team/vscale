import 'package:vscale_io/data/models/models.dart';
import 'package:vscale_io/domain/models/rplan_tarif_model.dart';
import 'package:vscale_io/domain/repositories/billing_repository.dart';
import 'package:vscale_io/domain/repositories/info_repository.dart';
import 'dart:async';

class InfoInteractor {
  InfoRepository repository;
  BillingRepository billingRepository;

  InfoInteractor(this.repository, this.billingRepository);

  Future<List<LocationEntity>> getLocations() {
    return repository.getLocations();
  }

  Future<LocationEntity> getLocationById(String id) {
    return repository.getLocationById(id);
  }

  Future<List<RplanEntity>> getRplans() {
    return repository.getRplans();
  }

  Future<RplanEntity> getRplanByName(String name) {
    return repository.getRplanByName(name);
  }

  Future<List<ImageEntity>> getImages() {
    return repository.getImages();
  }

  Future<ImageEntity> getImageById(String id) {
    return repository.getImageById(id);
  }

  Future<List<RplanTarifModel>> getRplansInfo() async {
    var plans = await repository.getRplans();
    var tarif = await billingRepository.getPrices();
    return plans.map((item) {
      var priceInfo = tarif.items[item.id];
      var priceItem = PriceItem.fromJson(priceInfo);
      return RplanTarifModel(rplanEntity: item, priceItem: priceItem);
    }).toList();
  }
}
