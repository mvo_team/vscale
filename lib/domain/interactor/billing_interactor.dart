import 'package:vscale_io/data/models/models.dart';
import 'package:vscale_io/domain/repositories/billing_repository.dart';

class BillingInteractor {
  BillingRepository _repository;

  BillingInteractor(this._repository);

  Future<BalanceEntity> getBalance() {
    return _repository.getBalance();
  }

  Future<PricesEntity> getPrices() {
    return _repository.getPrices();
  }
}
