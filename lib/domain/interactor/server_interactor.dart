import 'package:vscale_io/domain/repositories/server_repository.dart';
import 'dart:async';
import 'package:vscale_io/data/models/models.dart';

class ScaletInteractor {
  ScaletRepository _repository;

  ScaletInteractor(ScaletRepository rep) {
    this._repository = rep;
  }

  Future<List<ScaletEntity>> getListScalets() {
    return _repository.getScaletList();
  }

  Future<ScaletEntity> startServer(String ctid) {
    return _repository.startServer(ctid);
  }

  Future<ScaletEntity> restartServer(String ctid) {
    return _repository.restartServer(ctid);
  }

  Future<ScaletEntity> stopServer(String ctid) {
    return _repository.stopServer(ctid);

  }  Future<ScaletEntity> removeScale(String ctid) {
    return _repository.removeScale(ctid);
  }

  Future<ScaletEntity> getScaleInfo(String ctid) {
    return _repository.getScaleInfo(ctid);
  }

  Future<ScaletEntity> changeConfiguration(String ctid, String rplan) {
    return _repository.changeConfiguration(ctid, rplan);
  }
}
