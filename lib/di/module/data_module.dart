import 'package:http/http.dart' as http;
import 'package:vscale_io/data/converters.dart';
import 'package:vscale_io/data/fabric/string_factory.dart';
import 'package:vscale_io/data/models/models.dart';
import 'package:vscale_io/data/provider/network/impl/network_provider_impl.dart';
import 'package:vscale_io/data/provider/network/network_provider.dart';
import 'package:vscale_io/data/provider/prefs/impl/prefs_provider_impl.dart';
import 'package:vscale_io/data/provider/prefs/prefs_provider.dart';
import 'package:vscale_io/di/module/index.dart';
import 'package:vscale_io/domain/repositories/account_repository.dart';
import 'package:vscale_io/domain/repositories/backup_repository.dart';
import 'package:vscale_io/domain/repositories/billing_repository.dart';
import 'package:vscale_io/domain/repositories/impl/account_repository.dart';
import 'package:vscale_io/domain/repositories/impl/backup_repository.dart';
import 'package:vscale_io/domain/repositories/impl/billing_repository.dart';
import 'package:vscale_io/domain/repositories/impl/info_repository.dart';
import 'package:vscale_io/domain/repositories/impl/scalet_respository.dart';
import 'package:vscale_io/domain/repositories/info_repository.dart';
import 'package:vscale_io/domain/repositories/server_repository.dart';

class DataModule extends BaseModule {
  static DataModule _instance;

  static DataModule getInstance() {
    if (_instance == null) {
      _instance = DataModule();
    }
    return _instance;
  }

  DataModule() {
    register<EntityConverter>(EntityConverter.getInstance());

    register<http.Client>(http.Client());
    register<PrefsProvider>(PrefsProviderImpl());
    register<NetworkProvider>(NetworkProviderImpl(get(), get(), get()));
    register<AccountRepository>(AccountRepositoryImpl(get(), get()));
    register<ScaletRepository>(ScaletRepositoryImpl(get(), get()));
    register<BackupRepository>(BackupRepositoryImpl(get(), get()));
    register<BillingRepository>(BillingRepositoryImpl(get(), get()));
    register<InfoRepository>(InfoRepositoryImpl(get()));

    registerFabric();
  }

  void registerFabric() {
    EntityConverter converter = get();

    converter.register<AccountEntity>(AccountFabric());
    converter.register<AddressEntity>(AddressFactory());
    converter.register<BackupEntity>(BackupFabric());
    converter.register<BalanceEntity>(BalanceFactory());
    converter.register<KeyEntity>(KeyFactory());
    converter.register<ScaletEntity>(ScaleFabric());
    converter.register<SshKeyEntity>(SshKeyFabric());
    converter.register<TagEntity>(TagFactory());
    converter.register<TaskEntity>(TaskFabric());
    converter.register<UserTokenEntity>(UserTokenFactory());
    converter.register<String>(StringFactory());
    converter.register<LocationEntity>(LocationFactory());
    converter.register<RplanEntity>(RplanFactory());
    converter.register<ImageEntity>(ImageFactory());
    converter.register<PricesEntity>(PricesFactory());
    converter.register<PriceItem>(PriceItemFactory());
  }
}
