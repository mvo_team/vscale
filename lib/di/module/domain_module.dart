import 'package:vscale_io/di/module/index.dart';
import 'package:vscale_io/domain/interactor/billing_interactor.dart';
import 'package:vscale_io/domain/interactor/index.dart';
import 'package:vscale_io/domain/interactor/info_interactor.dart';

class DomainModule extends BaseModule {
  static DomainModule _instance;

  static DomainModule getInstance() {
    if (_instance == null) {
      _instance = DomainModule();
    }
    return _instance;
  }

  DomainModule() {
    register<AccountInteractor>(AccountInteractor(get()));
    register<BackupInteractor>(BackupInteractor(get()));
    register<ScaletInteractor>(ScaletInteractor(get()));
    register<BillingInteractor>(BillingInteractor(get()));
    register<InfoInteractor>(InfoInteractor(get(), get()));
  }
}
