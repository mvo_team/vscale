import 'package:get_it/get_it.dart';
import 'package:meta/meta.dart';

abstract class BaseModule {
  @protected
  static GetIt getIt = GetIt();

  T get<T>() {
    return getIt.get<T>();
  }

  @protected
  register<T>(T instance) {
    getIt.registerLazySingleton(() => instance);
  }
}
