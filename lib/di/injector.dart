import 'package:vscale_io/di/module/index.dart';

class Injector extends BaseModule {
  static Injector _instance;
  static var modules = {
    DataModule: () => DataModule.getInstance(),
    DomainModule: () => DomainModule.getInstance()
  };

  static T inject<T>() {
    if (_instance == null) {
      _instance = Injector();
    }
    return _instance.get<T>();
  }

  Injector() {
    modules.forEach((m, ints) => ints());
  }
}
