import 'package:vscale_io/di/injector.dart';

class ServiceLocator {
  static ServiceLocator _instance;

  static ServiceLocator getInstance() {
    if (_instance == null) {
      _instance = ServiceLocator();
    }
    return _instance;
  }

  static T getInteractor<T>() {
    return Injector.inject<T>();
  }
}
